package ru.renessans.jvschool.volkov.task.manager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class ProjectViewByIndexListener extends AbstractProjectListener {

    @NotNull
    private static final String CMD_PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    @NotNull
    private static final String DESC_PROJECT_VIEW_BY_INDEX = "просмотреть проект по индексу";

    @NotNull
    private static final String NOTIFY_PROJECT_VIEW_BY_INDEX =
            "Происходит попытка инициализации отображения проекта. \n" +
                    "Для отображения проекта по индексу введите индекс проекта из списка. ";

    public ProjectViewByIndexListener(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(projectEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_PROJECT_VIEW_BY_INDEX;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_PROJECT_VIEW_BY_INDEX;
    }

    @Async
    @Override
    @EventListener(condition = "@projectViewByIndexListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_PROJECT_VIEW_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @Nullable final ProjectDTO view = super.projectEndpoint.getProjectByIndex(current, index);
        ViewUtil.print(view);
    }

}