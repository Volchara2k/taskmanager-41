package ru.renessans.jvschool.volkov.task.manager.listener.user;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserEndpoint;

@RequiredArgsConstructor
public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    protected final UserEndpoint userEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

}