package ru.renessans.jvschool.volkov.task.manager.listener.security;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class UserSignInListener extends AbstractSecurityListener {

    @NotNull
    private static final String CMD_SIGN_IN = "sign-in";

    @NotNull
    private static final String DESC_SIGN_IN = "войти в систему";

    @NotNull
    private static final String NOTIFY_SIGN_IN =
            "Происходит попытка инициализации авторизации пользователя. \n" +
                    "Для авторизации пользователя в системе введите логин и пароль: ";

    public UserSignInListener(
            @NotNull final SessionEndpoint sessionEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(sessionEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_SIGN_IN;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_SIGN_IN;
    }

    @Async
    @Override
    @EventListener(condition = "@userSignInListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_SIGN_IN);
        @NotNull final String login = ViewUtil.getLine();
        @NotNull final String password = ViewUtil.getLine();
        @NotNull final SessionDTO open = super.sessionEndpoint.openSession(login, password);
        @NotNull final SessionDTO subscribe = super.currentSessionService.subscribe(open);
        ViewUtil.print(subscribe);
    }

}