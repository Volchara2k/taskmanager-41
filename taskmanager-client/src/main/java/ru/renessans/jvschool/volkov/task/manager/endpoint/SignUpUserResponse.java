package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for signUpUserResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="signUpUserResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="user" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}userLimitedDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "signUpUserResponse", propOrder = {
        "user"
})
public class SignUpUserResponse {

    protected UserLimitedDTO user;

    /**
     * Gets the value of the user property.
     *
     * @return possible object is
     * {@link UserLimitedDTO }
     */
    public UserLimitedDTO getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     *
     * @param value allowed object is
     *              {@link UserLimitedDTO }
     */
    public void setUser(UserLimitedDTO value) {
        this.user = value;
    }

}
