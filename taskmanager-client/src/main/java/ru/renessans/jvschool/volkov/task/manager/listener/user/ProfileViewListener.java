package ru.renessans.jvschool.volkov.task.manager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class ProfileViewListener extends AbstractUserListener {

    @NotNull
    private static final String CMD_VIEW_PROFILE = "view-profile";

    @NotNull
    private static final String DESC_VIEW_PROFILE = "обновить пароль пользователя";

    @NotNull
    private static final String NOTIFY_VIEW_PROFILE =
            "Происходит попытка инициализации отображения информации о текущем пользователе \n" +
                    "Информация о текущем профиле пользователя: ";

    public ProfileViewListener(
            @NotNull final UserEndpoint userEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(userEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_VIEW_PROFILE;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_VIEW_PROFILE;
    }

    @Async
    @Override
    @EventListener(condition = "@profileViewListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_VIEW_PROFILE);
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @Nullable final UserLimitedDTO view = super.userEndpoint.getUser(current);
        ViewUtil.print(view);
    }

}