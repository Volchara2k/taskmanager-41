package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.endpoint.*;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtilTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(IntegrationImplementation.class)
@Suite.SuiteClasses(
        {
                AdminDataInterChangeEndpointTest.class,
                AdminEndpointTest.class,
                AuthenticationEndpointTest.class,
                ProjectEndpointTest.class,
                SessionEndpointTest.class,
                TaskEndpointTest.class,
                UserEndpointTest.class
        }
)

public abstract class AbstractIntegrationImplementationRunner {
}