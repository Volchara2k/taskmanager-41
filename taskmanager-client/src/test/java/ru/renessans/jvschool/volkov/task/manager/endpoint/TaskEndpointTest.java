package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.util.Collection;
import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class TaskEndpointTest {

    @NotNull
    private static final SessionEndpointService SESSION_ENDPOINT_SERVICE = new SessionEndpointService();

    @NotNull
    private static final SessionEndpoint SESSION_ENDPOINT = SESSION_ENDPOINT_SERVICE.getSessionEndpointPort();

    @NotNull
    private static final TaskEndpointService TASK_ENDPOINT_SERVICE = new TaskEndpointService();

    @NotNull
    private static final TaskEndpoint TASK_ENDPOINT = TASK_ENDPOINT_SERVICE.getTaskEndpointPort();

    @BeforeAll
    static void assertMainComponentsBeforeAll() {
        Assert.assertNotNull(SESSION_ENDPOINT_SERVICE);
        Assert.assertNotNull(SESSION_ENDPOINT);
        Assert.assertNotNull(TASK_ENDPOINT_SERVICE);
        Assert.assertNotNull(TASK_ENDPOINT);
    }

    @Test
    @TestCaseName("Run testAddTask for addTask(session, random, random)")
    public void testAddTask() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTaskResponse = TASK_ENDPOINT.addTask(
                openSessionResponse, title, description
        );
        Assert.assertNotNull(addTaskResponse);
        Assert.assertEquals(title, addTaskResponse.getTitle());
        Assert.assertEquals(description, addTaskResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testUpdateTaskByIndex for updateTaskByIndex(session, 0, random, random)")
    public void testUpdateTaskByIndex() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        @Nullable final TaskDTO updateTaskResponse = TASK_ENDPOINT.updateTaskByIndex(
                openSessionResponse, 0, title, description
        );
        Assert.assertNotNull(updateTaskResponse);
        Assert.assertEquals(title, updateTaskResponse.getTitle());
        Assert.assertEquals(description, updateTaskResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testUpdateTaskById for updateTaskById(session, id, random, random)")
    public void testUpdateTaskById() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final TaskDTO taskResponse = TASK_ENDPOINT.getTaskByIndex(openSessionResponse, 0);
        Assert.assertNotNull(taskResponse);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        @Nullable final TaskDTO updateTaskResponse = TASK_ENDPOINT.updateTaskById(
                openSessionResponse, taskResponse.getId(), title, description
        );
        Assert.assertNotNull(updateTaskResponse);
        Assert.assertEquals(taskResponse.getId(), updateTaskResponse.getId());
        Assert.assertEquals(taskResponse.getUserId(), updateTaskResponse.getUserId());
        Assert.assertEquals(title, updateTaskResponse.getTitle());
        Assert.assertEquals(description, updateTaskResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testDeleteTaskById for deleteTaskById(session, id)")
    public void testDeleteTaskById() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_TEST_LOGIN, DemoDataConst.USER_TEST_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTaskResponse = TASK_ENDPOINT.addTask(openSessionResponse, title, description);
        Assert.assertNotNull(addTaskResponse);

        final int deleteTaskResponse = TASK_ENDPOINT.deleteTaskById(openSessionResponse, addTaskResponse.getId());
        Assert.assertEquals(1, deleteTaskResponse);
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testDeleteTaskByIndex for deleteTaskByIndex(session, 0)")
    public void testDeleteTaskByIndex() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTaskResponse = TASK_ENDPOINT.addTask(openSessionResponse, title, description);
        Assert.assertNotNull(addTaskResponse);
        @NotNull final TaskDTO taskResponse = TASK_ENDPOINT.getTaskByIndex(openSessionResponse, 0);
        Assert.assertNotNull(taskResponse);

        final int deleteTaskResponse = TASK_ENDPOINT.deleteTaskByIndex(openSessionResponse, 0);
        Assert.assertEquals(1, deleteTaskResponse);
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testDeleteTaskByTitle for deleteTaskByTitle(session, title)")
    public void testDeleteTaskByTitle() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTaskResponse = TASK_ENDPOINT.addTask(openSessionResponse, title, description);
        Assert.assertNotNull(addTaskResponse);

        final int deleteTaskResponse = TASK_ENDPOINT.deleteTaskByTitle(openSessionResponse, addTaskResponse.getTitle());
        Assert.assertEquals(1, deleteTaskResponse);
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testDeleteAllTasks for deleteAllTasks(session)")
    public void testDeleteAllTasks() {
        @NotNull final SessionDTO openSession = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(openSession);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTaskResponse = TASK_ENDPOINT.addTask(openSession, title, description);
        Assert.assertNotNull(addTaskResponse);

        final int deleteTaskResponse = TASK_ENDPOINT.deleteAllTasks(openSession);
        Assert.assertEquals(1, deleteTaskResponse);
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSession);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testGetTaskById for getTaskById(session, id)")
    public void testGetTaskById() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTask = TASK_ENDPOINT.addTask(openSessionResponse, title, description);
        Assert.assertNotNull(addTask);

        @Nullable final TaskDTO taskResponse = TASK_ENDPOINT.getTaskById(openSessionResponse, addTask.getId());
        Assert.assertNotNull(taskResponse);
        Assert.assertEquals(addTask.getId(), taskResponse.getId());
        Assert.assertEquals(addTask.getUserId(), taskResponse.getUserId());
        Assert.assertEquals(addTask.getTitle(), taskResponse.getTitle());
        Assert.assertEquals(addTask.getDescription(), taskResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testGetTaskByIndex for getTaskByIndex(session, 0)")
    public void testGetTaskByIndex() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final TaskDTO taskResponse = TASK_ENDPOINT.getTaskByIndex(openSessionResponse, 0);
        Assert.assertNotNull(taskResponse);
        Assert.assertEquals(openSessionResponse.getUserId(), taskResponse.getUserId());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testGetTaskByTitle for getTaskByTitle(session, title)")
    public void testGetTaskByTitle() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTask = TASK_ENDPOINT.addTask(openSessionResponse, title, description);
        Assert.assertNotNull(addTask);

        @Nullable final TaskDTO taskResponse = TASK_ENDPOINT.getTaskByTitle(openSessionResponse, addTask.getTitle());
        Assert.assertNotNull(taskResponse);
        Assert.assertEquals(addTask.getId(), taskResponse.getId());
        Assert.assertEquals(addTask.getUserId(), taskResponse.getUserId());
        Assert.assertEquals(addTask.getTitle(), taskResponse.getTitle());
        Assert.assertEquals(addTask.getDescription(), taskResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testGetAllTasks for getAllTasks(session)")
    public void testGetAllTasks() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final Collection<TaskDTO> allTasksResponse = TASK_ENDPOINT.getAllTasks(openSessionResponse);
        Assert.assertNotNull(allTasksResponse);
        Assert.assertNotEquals(0, allTasksResponse.size());
        final boolean isUserTasks = allTasksResponse.stream().allMatch(
                entity -> openSessionResponse.getUserId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

}