package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;

import java.util.Collection;
import java.util.Collections;

public final class CommandRepositoryTest {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository(Collections.emptyList());

    @Test
    @TestCaseName("Run testNegativeInitialCommands for getAllCommands()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetAllCommands() {
        Assert.assertNotNull(this.commandRepository);
        @Nullable final Collection<String> allCommands = this.commandRepository.getAllCommands();
        Assert.assertNotNull(allCommands);
        Assert.assertNotEquals(0, allCommands.size());
    }

    @Test
    @TestCaseName("Run testNegativeInitialCommands for getAllTerminalCommands()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetAllTerminalCommands() {
        Assert.assertNotNull(this.commandRepository);
        @Nullable final Collection<String> allTerminalCommands = this.commandRepository.getAllTerminalCommands();
        Assert.assertNotNull(allTerminalCommands);
        Assert.assertNotEquals(0, allTerminalCommands.size());
    }

    @Test
    @TestCaseName("Run testNegativeInitialCommands for getAllArgumentCommands()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetAllArgumentCommands() {
        Assert.assertNotNull(this.commandRepository);
        @Nullable final Collection<String> allArgumentCommands = this.commandRepository.getAllArgumentCommands();
        Assert.assertNotNull(allArgumentCommands);
        Assert.assertNotEquals(0, allArgumentCommands.size());
    }

}