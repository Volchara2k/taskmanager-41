package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.InternalDangerImplementation;

import java.util.Collection;
import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class AdminEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();


    @NotNull
    private final AuthenticationEndpointService authenticationEndpointService = new AuthenticationEndpointService();

    @NotNull
    private final AuthenticationEndpoint authEndpoint = authenticationEndpointService.getAuthenticationEndpointPort();


    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @Test
    @TestCaseName("Run testCloseAllSessions for closeAllSessions(session)")
    public void testCloseAllSessions() {
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final int closeSessions = this.adminEndpoint.closedAllSessions(openSessionResponse);
        Assert.assertNotEquals(0, closeSessions);
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testGetAllSessions for getAllSessions(session)")
    public void testGetAllSessions() {
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final Collection<SessionDTO> allSessions = this.adminEndpoint.getAllSessions(openSessionResponse);
        Assert.assertNotNull(allSessions);
        Assert.assertNotEquals(0, allSessions.size());
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testSignUpUserWithUserRole for signUpUserWithUserRole(session, random, random)")
    public void testSignUpUserWithUserRole() {
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUser = this.adminEndpoint.signUpUserWithUserRole(
                openSessionResponse, login, password, UserRole.USER
        );
        Assert.assertNotNull(addUser);
        Assert.assertEquals(login, addUser.getLogin());
        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testDeleteUserById for deleteUserById(session, id)")
    public void testDeleteUserById() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final int deleteFlag = this.adminEndpoint.deleteUserById(openSessionResponse, addRecord.getId());
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testDeleteUserByLogin for deleteUserByLogin(session, login)")
    public void testDeleteUserByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @Ignore("Делает работу остальных тестов некорректной")
    @Category(InternalDangerImplementation.class)
    @TestCaseName("Run testDeleteAllUsers for deleteAllUsers(session)")
    public void testDeleteAllUsers() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final int deletedAll = this.adminEndpoint.deleteAllUsers(openSessionResponse);
        Assert.assertEquals(1, deletedAll);
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testLockUserByLogin for lockUserByLogin(session, login)")
    public void testLockUserByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO lockUser = this.adminEndpoint.lockUserByLogin(openSessionResponse, login);
        Assert.assertNotNull(lockUser);
        Assert.assertEquals(addRecord.getId(), lockUser.getId());
        Assert.assertEquals(login, lockUser.getLogin());
        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testUnlockUserByLogin for unlockUserByLogin(session, login)")
    public void testUnlockUserByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @Nullable final UserLimitedDTO lockUser = this.adminEndpoint.lockUserByLogin(openSessionResponse, login);
        Assert.assertNotNull(lockUser);

        @Nullable final UserLimitedDTO unlockUser = this.adminEndpoint.unlockUserByLogin(openSessionResponse, login);
        Assert.assertNotNull(unlockUser);
        Assert.assertEquals(addRecord.getId(), unlockUser.getId());
        Assert.assertEquals(login, unlockUser.getLogin());
        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testGetAllUsers for getAllUsers(session)")
    public void testGetAllUsers() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final Collection<UserLimitedDTO> getUser = this.adminEndpoint.getAllUsers(openSessionResponse);
        Assert.assertNotNull(getUser);
        Assert.assertNotEquals(0, getUser.size());
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testGetAllUsersTasks for getAllUsersTasks(session)")
    public void testGetAllUsersTasks() {
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final Collection<TaskDTO> allUsersTasks = this.adminEndpoint.getAllUsersTasks(openSessionResponse);
        Assert.assertNotNull(allUsersTasks);
        Assert.assertNotEquals(0, allUsersTasks.size());
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testGetAllUsersProjects for getAllUsersProjects(session)")
    public void testGetAllUsersProjects() {
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final Collection<ProjectDTO> allUsersProjects = this.adminEndpoint.getAllUsersProjects(openSessionResponse);
        Assert.assertNotNull(allUsersProjects);
        Assert.assertNotEquals(0, allUsersProjects.size());
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testGetUserById for getUserById(session, login)")
    public void testGetUserById() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO getUser = this.adminEndpoint.getUserById(openSessionResponse, addRecord.getId());
        Assert.assertNotNull(getUser);
        Assert.assertEquals(addRecord.getId(), getUser.getId());
        Assert.assertEquals(login, getUser.getLogin());
        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testGetUserByLogin for getUserByLogin(session, login)")
    public void testGetUserByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO getUser = this.adminEndpoint.getUserByLogin(openSessionResponse, addRecord.getLogin());
        Assert.assertNotNull(getUser);
        Assert.assertEquals(addRecord.getId(), getUser.getId());
        Assert.assertEquals(login, getUser.getLogin());
        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testEditProfileById for editProfileById(session, id, \"newFirstName\")")
    public void testEditProfileById() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO editUser = this.adminEndpoint.editProfileById(openSessionResponse, addRecord.getId(), "newFirstName");
        Assert.assertNotNull(editUser);
        Assert.assertEquals(addRecord.getId(), editUser.getId());
        Assert.assertEquals(login, editUser.getLogin());
        Assert.assertEquals("newFirstName", editUser.getFirstName());
        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testUpdatePasswordById for updatePasswordById(session, id, \"newPassword\")")
    public void testUpdatePasswordById() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO openSessionResponse = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO updateUserResponse = this.adminEndpoint.updatePasswordById(openSessionResponse, addRecord.getId(), "newPassword");
        Assert.assertNotNull(updateUserResponse);
        Assert.assertEquals(addRecord.getId(), updateUserResponse.getId());
        Assert.assertEquals(login, updateUserResponse.getLogin());
        final int deleteUserResponse = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteUserResponse);
        @Nullable final SessionDTO closeSessionResponse = this.sessionEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

}