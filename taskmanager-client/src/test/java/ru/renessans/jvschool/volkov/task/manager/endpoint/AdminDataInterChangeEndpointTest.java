package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class AdminDataInterChangeEndpointTest {

    @NotNull
    private static final SessionEndpointService SESSION_ENDPOINT_SERVICE = new SessionEndpointService();

    @NotNull
    private static final SessionEndpoint SESSION_ENDPOINT = SESSION_ENDPOINT_SERVICE.getSessionEndpointPort();

    @NotNull
    private static final AdminDataInterChangeEndpointService ADMIN_DATA_INTER_CHANGE_ENDPOINT_SERVICE = new AdminDataInterChangeEndpointService();

    @NotNull
    private static final AdminDataInterChangeEndpoint DATA_ENDPOINT = ADMIN_DATA_INTER_CHANGE_ENDPOINT_SERVICE.getAdminDataInterChangeEndpointPort();

    @BeforeAll
    static void assertMainComponentsBeforeAll() {
        Assert.assertNotNull(SESSION_ENDPOINT_SERVICE);
        Assert.assertNotNull(SESSION_ENDPOINT);
        Assert.assertNotNull(ADMIN_DATA_INTER_CHANGE_ENDPOINT_SERVICE);
        Assert.assertNotNull(DATA_ENDPOINT);
    }
    @BeforeEach
    @SneakyThrows
    public void createFilesBefore() {
        Files.deleteIfExists(Paths.get(DemoDataConst.BIN_LOCATE));
        Files.createFile(new File(DemoDataConst.BIN_LOCATE).toPath());
        Files.deleteIfExists(Paths.get(DemoDataConst.BASE64_LOCATE));
        Files.createFile(new File(DemoDataConst.BASE64_LOCATE).toPath());
        Files.deleteIfExists(Paths.get(DemoDataConst.JSON_LOCATE));
        Files.createFile(new File(DemoDataConst.JSON_LOCATE).toPath());
        Files.deleteIfExists(Paths.get(DemoDataConst.XML_LOCATE));
        Files.createFile(new File(DemoDataConst.XML_LOCATE).toPath());
        Files.deleteIfExists(Paths.get(DemoDataConst.YAML_LOCATE));
        Files.createFile(new File(DemoDataConst.YAML_LOCATE).toPath());
    }

    @AfterAll
    @SneakyThrows
    public static void deleteFilesAfter() {
        Files.deleteIfExists(Paths.get(DemoDataConst.BIN_LOCATE));
        Files.deleteIfExists(Paths.get(DemoDataConst.BASE64_LOCATE));
        Files.deleteIfExists(Paths.get(DemoDataConst.JSON_LOCATE));
        Files.deleteIfExists(Paths.get(DemoDataConst.XML_LOCATE));
        Files.deleteIfExists(Paths.get(DemoDataConst.YAML_LOCATE));
    }

    @Test
    @TestCaseName("Run testDataBinClear for dataBinClear(session)")
    @SneakyThrows
    public void testDataBinClear() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final boolean clearResponse = DATA_ENDPOINT.dataBinClear(openSessionResponse);
        Assert.assertTrue(clearResponse);
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testDataBase64Clear for dataBase64Clear(session)")
    @SneakyThrows
    public void testDataBase64Clear() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final boolean clearResponse = DATA_ENDPOINT.dataBase64Clear(openSessionResponse);
        Assert.assertTrue(clearResponse);
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testDataJsonClear for dataJsonClear(session)")
    public void testDataJsonClear() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final boolean clearResponse = DATA_ENDPOINT.dataJsonClear(openSessionResponse);
        Assert.assertTrue(clearResponse);
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testDataXmlClear for dataXmlClear(session)")
    public void testDataXmlClear() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final boolean clearResponse = DATA_ENDPOINT.dataXmlClear(openSessionResponse);
        Assert.assertTrue(clearResponse);
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testDataYamlClear for dataYamlClear(session)")
    public void testDataYamlClear() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final boolean clearResponse = DATA_ENDPOINT.dataYamlClear(openSessionResponse);
        Assert.assertTrue(clearResponse);
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testExportDataBin for exportDataBin(session)")
    public void testExportDataBin() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final DomainDTO exportResponse = DATA_ENDPOINT.exportDataBin(openSessionResponse);
        Assert.assertNotNull(exportResponse);
        Assert.assertNotEquals(0, exportResponse.getProjects().size());
        Assert.assertNotEquals(0, exportResponse.getTasks().size());
        Assert.assertNotEquals(0, exportResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testExportDataBase64 for exportDataBase64(session)")
    public void testExportDataBase64() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final DomainDTO exportResponse = DATA_ENDPOINT.exportDataBase64(openSessionResponse);
        Assert.assertNotNull(exportResponse);
        Assert.assertNotEquals(0, exportResponse.getProjects().size());
        Assert.assertNotEquals(0, exportResponse.getTasks().size());
        Assert.assertNotEquals(0, exportResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testExportDataJson for exportDataJson(session)")
    public void testExportDataJson() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final DomainDTO exportResponse = DATA_ENDPOINT.exportDataJson(openSessionResponse);
        Assert.assertNotNull(exportResponse);
        Assert.assertNotEquals(0, exportResponse.getProjects().size());
        Assert.assertNotEquals(0, exportResponse.getTasks().size());
        Assert.assertNotEquals(0, exportResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testExportDataXml for exportDataXml(session)")
    public void testExportDataXml() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final DomainDTO exportResponse = DATA_ENDPOINT.exportDataXml(openSessionResponse);
        Assert.assertNotNull(exportResponse);
        Assert.assertNotEquals(0, exportResponse.getProjects().size());
        Assert.assertNotEquals(0, exportResponse.getTasks().size());
        Assert.assertNotEquals(0, exportResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testExportDataYaml for exportDataYaml(session)")
    public void testExportDataYaml() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final DomainDTO exportResponse = DATA_ENDPOINT.exportDataYaml(openSessionResponse);
        Assert.assertNotNull(exportResponse);
        Assert.assertNotEquals(0, exportResponse.getProjects().size());
        Assert.assertNotEquals(0, exportResponse.getTasks().size());
        Assert.assertNotEquals(0, exportResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testImportDataBin for importDataBin(open)")
    public void testImportDataBin() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final DomainDTO exportResponse = DATA_ENDPOINT.exportDataJson(openSessionResponse);
        Assert.assertNotNull(exportResponse);

        @NotNull final DomainDTO importResponse = DATA_ENDPOINT.importDataBin(openSessionResponse);
        Assert.assertNotNull(importResponse);
        Assert.assertNotEquals(0, importResponse.getProjects().size());
        Assert.assertNotEquals(0, importResponse.getTasks().size());
        Assert.assertNotEquals(0, importResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testImportDataBase64 for importDataBase64(session)")
    public void testImportDataBase64() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final DomainDTO exportResponse = DATA_ENDPOINT.exportDataBase64(openSessionResponse);
        Assert.assertNotNull(exportResponse);

        @NotNull final DomainDTO importResponse = DATA_ENDPOINT.importDataBase64(openSessionResponse);
        Assert.assertNotNull(importResponse);
        Assert.assertNotEquals(0, importResponse.getProjects().size());
        Assert.assertNotEquals(0, importResponse.getTasks().size());
        Assert.assertNotEquals(0, importResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testImportDataJson for importDataJson(session)")
    public void testImportDataJson() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final DomainDTO exportResponse = DATA_ENDPOINT.exportDataJson(openSessionResponse);
        Assert.assertNotNull(exportResponse);

        @NotNull final DomainDTO importResponse = DATA_ENDPOINT.importDataJson(openSessionResponse);
        Assert.assertNotNull(importResponse);
        Assert.assertNotEquals(0, importResponse.getProjects().size());
        Assert.assertNotEquals(0, importResponse.getTasks().size());
        Assert.assertNotEquals(0, importResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testImportDataXml for importDataXml(session)")
    public void testImportDataXml() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final DomainDTO exportResponse = DATA_ENDPOINT.exportDataXml(openSessionResponse);
        Assert.assertNotNull(exportResponse);

        @NotNull final DomainDTO importResponse = DATA_ENDPOINT.importDataXml(openSessionResponse);
        Assert.assertNotNull(importResponse);
        Assert.assertNotEquals(0, importResponse.getProjects().size());
        Assert.assertNotEquals(0, importResponse.getTasks().size());
        Assert.assertNotEquals(0, importResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testImportDataYaml for importDataYaml(session)")
    public void testImportDataYaml() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final DomainDTO exportResponse = DATA_ENDPOINT.exportDataYaml(openSessionResponse);
        Assert.assertNotNull(exportResponse);

        @NotNull final DomainDTO importResponse = DATA_ENDPOINT.importDataYaml(openSessionResponse);
        Assert.assertNotNull(importResponse);
        Assert.assertNotEquals(0, importResponse.getProjects().size());
        Assert.assertNotEquals(0, importResponse.getTasks().size());
        Assert.assertNotEquals(0, importResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

}