package ru.renessans.jvschool.volkov.task.manager;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.BeforeAll;
import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataApiProvider;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.util.List;

/**
 * API tests for DefaultApi
 */
@Category(IntegrationImplementation.class)
public class DefaultApiTest {

    @NotNull
    private static final DefaultApi API = new DefaultApi();

    @NotNull
    private static final CaseDataApiProvider DATA_BUILDER = new CaseDataApiProvider();

    @BeforeAll
    static void assertCMainComponentsBeforeAll() {
        Assert.assertNotNull(API);
        Assert.assertNotNull(DATA_BUILDER);
    }

    /**
     * Create project
     * <p>
     * Returns created project. Created project required.
     */
    @Test
    public void createProjectTest() {
        @NotNull final ProjectDTO projectBodyDTO = DATA_BUILDER.createProjectDTO();
        Assert.assertNotNull(projectBodyDTO);
        Assert.assertNotNull(projectBodyDTO.getTimeFrame());

        @NotNull final ProjectDTO createProjectResponse = API.createProject(projectBodyDTO);
        Assert.assertNotNull(createProjectResponse);
        Assert.assertEquals(projectBodyDTO.getId(), createProjectResponse.getId());
        Assert.assertEquals(projectBodyDTO.getUserId(), createProjectResponse.getUserId());
        Assert.assertEquals(projectBodyDTO.getTitle(), createProjectResponse.getTitle());
        Assert.assertEquals(projectBodyDTO.getDescription(), createProjectResponse.getDescription());
        Assert.assertEquals(projectBodyDTO.getStatus(), createProjectResponse.getStatus());
        Assert.assertEquals(projectBodyDTO.getTimeFrame(), createProjectResponse.getTimeFrame());
        final int deleteProjectResponse = API.deleteProjectById(createProjectResponse.getId());
        Assert.assertEquals(1, deleteProjectResponse);
    }

    /**
     * Create task
     * <p>
     * Returns created task. Created task required.
     */
    @Test
    public void createTaskTest() {
        final TaskDTO taskBodyDTO = DATA_BUILDER.createTaskDTO();
        Assert.assertNotNull(taskBodyDTO);
        Assert.assertNotNull(taskBodyDTO.getTimeFrame());

        final TaskDTO createTaskResponse = API.createTask(taskBodyDTO);
        Assert.assertNotNull(createTaskResponse);
        Assert.assertEquals(taskBodyDTO.getId(), createTaskResponse.getId());
        Assert.assertEquals(taskBodyDTO.getUserId(), createTaskResponse.getUserId());
        Assert.assertEquals(taskBodyDTO.getTitle(), createTaskResponse.getTitle());
        Assert.assertEquals(taskBodyDTO.getDescription(), createTaskResponse.getDescription());
        Assert.assertEquals(taskBodyDTO.getStatus(), createTaskResponse.getStatus());
        Assert.assertEquals(taskBodyDTO.getTimeFrame(), createTaskResponse.getTimeFrame());
        final int deleteTaskResponse = API.deleteTaskById(createTaskResponse.getId());
        Assert.assertEquals(1, deleteTaskResponse);
    }

    /**
     * Delete project by id
     * <p>
     * Returns integer deleted flag: 1 - true, 0 - false. Unique ID required.
     */
    @Test
    public void deleteProjectByIdTest() {
        @NotNull final ProjectDTO projectBodyDTO = DATA_BUILDER.createProjectDTO();
        Assert.assertNotNull(projectBodyDTO);
        Assert.assertNotNull(projectBodyDTO.getTimeFrame());
        @NotNull final ProjectDTO addProjectResponse = API.createProject(projectBodyDTO);
        Assert.assertNotNull(addProjectResponse);

        final int deleteProjectResponse = API.deleteProjectById(projectBodyDTO.getId());
        Assert.assertEquals(1, deleteProjectResponse);
    }

    /**
     * Delete task by id
     * <p>
     * Returns integer deleted flag: 1 - true, 0 - false. Unique ID required.
     */
    @Test
    public void deleteTaskByIdTest() {
        @NotNull final TaskDTO taskBodyDTO = DATA_BUILDER.createTaskDTO();
        Assert.assertNotNull(taskBodyDTO);
        Assert.assertNotNull(taskBodyDTO.getTimeFrame());
        @NotNull final TaskDTO addTaskResponse = API.createTask(taskBodyDTO);
        Assert.assertNotNull(addTaskResponse);

        final int deleteTaskResponse = API.deleteTaskById(taskBodyDTO.getId());
        Assert.assertEquals(1, deleteTaskResponse);
    }

    /**
     * Edit project
     * <p>
     * Returns edited project. Edited project required.
     */
    @Test
    public void editProjectTest() {
        @NotNull final ProjectDTO projectBodyDTO = DATA_BUILDER.createProjectDTO();
        Assert.assertNotNull(projectBodyDTO);
        Assert.assertNotNull(projectBodyDTO.getTimeFrame());
        @NotNull final ProjectDTO addProjectResponse = API.createProject(projectBodyDTO);
        Assert.assertNotNull(addProjectResponse);
        @NotNull final String newTitle = "new title";
        projectBodyDTO.setTitle(newTitle);

        @NotNull final ProjectDTO editProjectResponse = API.editProject(projectBodyDTO);
        Assert.assertNotNull(editProjectResponse);
        Assert.assertEquals(projectBodyDTO.getId(), editProjectResponse.getId());
        Assert.assertEquals(projectBodyDTO.getUserId(), editProjectResponse.getUserId());
        Assert.assertEquals(newTitle, editProjectResponse.getTitle());
        Assert.assertEquals(projectBodyDTO.getDescription(), editProjectResponse.getDescription());
        Assert.assertEquals(projectBodyDTO.getStatus(), editProjectResponse.getStatus());
        Assert.assertEquals(projectBodyDTO.getTimeFrame(), editProjectResponse.getTimeFrame());
        final int deleteProjectResponse = API.deleteProjectById(editProjectResponse.getId());
        Assert.assertEquals(1, deleteProjectResponse);
    }

    /**
     * Edit task
     * <p>
     * Returns edited task. Edited task required.
     */
    @Test
    public void editTaskTest() {
        @NotNull final TaskDTO taskBodyDTO = DATA_BUILDER.createTaskDTO();
        Assert.assertNotNull(taskBodyDTO);
        Assert.assertNotNull(taskBodyDTO.getTimeFrame());
        @NotNull final TaskDTO addTaskResponse = API.createTask(taskBodyDTO);
        Assert.assertNotNull(addTaskResponse);
        @NotNull final String newTitle = "new title";
        taskBodyDTO.setTitle(newTitle);

        @NotNull final TaskDTO editTaskResponse = API.editTask(taskBodyDTO);
        Assert.assertNotNull(editTaskResponse);
        Assert.assertEquals(taskBodyDTO.getId(), editTaskResponse.getId());
        Assert.assertEquals(taskBodyDTO.getUserId(), editTaskResponse.getUserId());
        Assert.assertEquals(newTitle, editTaskResponse.getTitle());
        Assert.assertEquals(taskBodyDTO.getDescription(), editTaskResponse.getDescription());
        Assert.assertEquals(taskBodyDTO.getStatus(), editTaskResponse.getStatus());
        Assert.assertEquals(taskBodyDTO.getTimeFrame(), editTaskResponse.getTimeFrame());
        final int deleteTaskResponse = API.deleteTaskById(editTaskResponse.getId());
        Assert.assertEquals(1, deleteTaskResponse);
    }

    /**
     * Get all projects
     * <p>
     * Returns a complete list of project details by order of creation.
     */
    @Test
    public void getAllProjectsTest() {
        @NotNull final ProjectDTO projectBodyDTO = DATA_BUILDER.createProjectDTO();
        Assert.assertNotNull(projectBodyDTO);
        Assert.assertNotNull(projectBodyDTO.getTimeFrame());
        @NotNull final ProjectDTO addProjectResponse = API.createProject(projectBodyDTO);
        Assert.assertNotNull(addProjectResponse);

        @NotNull final List<ProjectDTO> allProjectsResponse = API.getAllProjects();
        Assert.assertNotNull(allProjectsResponse);
        Assert.assertNotEquals(0, allProjectsResponse.size());
        final int deleteProjectResponse = API.deleteProjectById(projectBodyDTO.getId());
        Assert.assertEquals(1, deleteProjectResponse);
    }

    /**
     * Get all projects
     * <p>
     * Returns a complete list of projects details by order of creation.
     */
    @Test
    public void getAllTasksTest() {
        @NotNull final TaskDTO taskBodyDTO = DATA_BUILDER.createTaskDTO();
        Assert.assertNotNull(taskBodyDTO);
        Assert.assertNotNull(taskBodyDTO.getTimeFrame());
        @NotNull final TaskDTO addTaskResponse = API.createTask(taskBodyDTO);
        Assert.assertNotNull(addTaskResponse);

        @NotNull final List<TaskDTO> allTasksResponse = API.getAllTasks();
        Assert.assertNotNull(allTasksResponse);
        Assert.assertNotEquals(0, allTasksResponse.size());
        final int deleteTaskResponse = API.deleteTaskById(taskBodyDTO.getId());
        Assert.assertEquals(1, deleteTaskResponse);
    }

    /**
     * Get project by ID
     * <p>
     * Returns project by unique ID. Unique ID required.
     */
    @Test
    public void getProjectByIdTest() {
        @NotNull final ProjectDTO projectBodyDTO = DATA_BUILDER.createProjectDTO();
        Assert.assertNotNull(projectBodyDTO);
        Assert.assertNotNull(projectBodyDTO.getTimeFrame());
        @NotNull final ProjectDTO addProjectResponse = API.createProject(projectBodyDTO);
        Assert.assertNotNull(addProjectResponse);

        @NotNull final ProjectDTO projectResponse = API.getProjectById(projectBodyDTO.getId());
        Assert.assertNotNull(projectResponse);
        Assert.assertEquals(projectBodyDTO.getId(), projectResponse.getId());
        Assert.assertEquals(projectBodyDTO.getUserId(), projectResponse.getUserId());
        Assert.assertEquals(projectBodyDTO.getTitle(), projectResponse.getTitle());
        Assert.assertEquals(projectBodyDTO.getDescription(), projectResponse.getDescription());
        Assert.assertEquals(projectBodyDTO.getStatus(), projectResponse.getStatus());
        final int deleteProjectResponse = API.deleteProjectById(projectResponse.getId());
        Assert.assertEquals(1, deleteProjectResponse);
    }

    /**
     * Get task by ID
     * <p>
     * Returns task by unique ID. Unique ID required.
     */
    @Test
    public void getTaskByIdTest() {
        @NotNull final TaskDTO taskBodyDTO = DATA_BUILDER.createTaskDTO();
        Assert.assertNotNull(taskBodyDTO);
        Assert.assertNotNull(taskBodyDTO.getTimeFrame());
        @NotNull final TaskDTO addTaskResponse = API.createTask(taskBodyDTO);
        Assert.assertNotNull(addTaskResponse);

        @NotNull final TaskDTO taskResponse = API.getTaskById(taskBodyDTO.getId());
        Assert.assertNotNull(taskResponse);
        Assert.assertEquals(taskBodyDTO.getId(), taskResponse.getId());
        Assert.assertEquals(taskBodyDTO.getUserId(), taskResponse.getUserId());
        Assert.assertEquals(taskBodyDTO.getTitle(), taskResponse.getTitle());
        Assert.assertEquals(taskBodyDTO.getDescription(), taskResponse.getDescription());
        Assert.assertEquals(taskBodyDTO.getStatus(), taskResponse.getStatus());
        final int deleteProjectResponse = API.deleteTaskById(taskResponse.getId());
        Assert.assertEquals(1, deleteProjectResponse);
    }

}