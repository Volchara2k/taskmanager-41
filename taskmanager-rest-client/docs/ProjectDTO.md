
# ProjectDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Unique ID |  [optional]
**title** | **String** | Title | 
**description** | **String** | Description | 
**userId** | **String** | Unique ID of user |  [optional]
**timeFrame** | [**TimeFrameDTO**](TimeFrameDTO.md) | Time frame | 
**status** | [**StatusEnum**](#StatusEnum) | Status | 


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
NOT_STARTED | &quot;NOT_STARTED&quot;
IN_PROGRESS | &quot;IN_PROGRESS&quot;
COMPLETED | &quot;COMPLETED&quot;



