# DefaultApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProject**](DefaultApi.md#createProject) | **PUT** /api/projects/project/create | Create project
[**createProject_0**](DefaultApi.md#createProject_0) | **POST** /api/projects/project/create | Create project
[**createTask**](DefaultApi.md#createTask) | **PUT** /api/tasks/task/create | Create task
[**createTask_0**](DefaultApi.md#createTask_0) | **POST** /api/tasks/task/create | Create task
[**deleteProjectById**](DefaultApi.md#deleteProjectById) | **DELETE** /api/projects/project/delete/{id} | Delete project by id
[**deleteTaskById**](DefaultApi.md#deleteTaskById) | **DELETE** /api/tasks/task/delete/{id} | Delete task by id
[**editProject**](DefaultApi.md#editProject) | **PUT** /api/projects/project/edit | Edit project
[**editProject_0**](DefaultApi.md#editProject_0) | **POST** /api/projects/project/edit | Edit project
[**editTask**](DefaultApi.md#editTask) | **PUT** /api/tasks/task/edit | Edit task
[**editTask_0**](DefaultApi.md#editTask_0) | **POST** /api/tasks/task/edit | Edit task
[**getAllProjects**](DefaultApi.md#getAllProjects) | **GET** /api/projects | Get all projects
[**getAllTasks**](DefaultApi.md#getAllTasks) | **GET** /api/tasks | Get all projects
[**getProjectById**](DefaultApi.md#getProjectById) | **GET** /api/projects/project/view/{id} | Get project by ID
[**getTaskById**](DefaultApi.md#getTaskById) | **GET** /api/tasks/task/view/{id} | Get task by ID


<a name="createProject"></a>
# **createProject**
> ProjectDTO createProject(projectDTO)

Create project

Returns created project. Created project required.

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDTO projectDTO = new ProjectDTO(); // ProjectDTO | Created project
try {
    ProjectDTO result = apiInstance.createProject(projectDTO);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectDTO** | [**ProjectDTO**](ProjectDTO.md)| Created project |

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createProject_0"></a>
# **createProject_0**
> ProjectDTO createProject_0(projectDTO)

Create project

Returns created project. Created project required.

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDTO projectDTO = new ProjectDTO(); // ProjectDTO | Created project
try {
    ProjectDTO result = apiInstance.createProject_0(projectDTO);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createProject_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectDTO** | [**ProjectDTO**](ProjectDTO.md)| Created project |

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createTask"></a>
# **createTask**
> TaskDTO createTask(taskDTO)

Create task

Returns created task. Created task required.

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDTO taskDTO = new TaskDTO(); // TaskDTO | Created project
try {
    TaskDTO result = apiInstance.createTask(taskDTO);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createTask");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskDTO** | [**TaskDTO**](TaskDTO.md)| Created project |

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createTask_0"></a>
# **createTask_0**
> TaskDTO createTask_0(taskDTO)

Create task

Returns created task. Created task required.

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDTO taskDTO = new TaskDTO(); // TaskDTO | Created project
try {
    TaskDTO result = apiInstance.createTask_0(taskDTO);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createTask_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskDTO** | [**TaskDTO**](TaskDTO.md)| Created project |

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteProjectById"></a>
# **deleteProjectById**
> Integer deleteProjectById(id)

Delete project by id

Returns integer deleted flag: 1 - true, 0 - false. Unique ID required.

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "\"7ca5a0e6-5f22-4955-b134-42f826a2d8dc\""; // String | Unique ID of project
try {
    Integer result = apiInstance.deleteProjectById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteProjectById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Unique ID of project |

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteTaskById"></a>
# **deleteTaskById**
> Integer deleteTaskById(id)

Delete task by id

Returns integer deleted flag: 1 - true, 0 - false. Unique ID required.

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "\"7ca5a0e6-5f22-4955-b134-42f826a2d8dc\""; // String | Unique ID of task
try {
    Integer result = apiInstance.deleteTaskById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteTaskById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Unique ID of task |

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="editProject"></a>
# **editProject**
> ProjectDTO editProject(projectDTO)

Edit project

Returns edited project. Edited project required.

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDTO projectDTO = new ProjectDTO(); // ProjectDTO | Edited project
try {
    ProjectDTO result = apiInstance.editProject(projectDTO);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#editProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectDTO** | [**ProjectDTO**](ProjectDTO.md)| Edited project |

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="editProject_0"></a>
# **editProject_0**
> ProjectDTO editProject_0(projectDTO)

Edit project

Returns edited project. Edited project required.

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDTO projectDTO = new ProjectDTO(); // ProjectDTO | Edited project
try {
    ProjectDTO result = apiInstance.editProject_0(projectDTO);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#editProject_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectDTO** | [**ProjectDTO**](ProjectDTO.md)| Edited project |

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="editTask"></a>
# **editTask**
> TaskDTO editTask(taskDTO)

Edit task

Returns edited task. Edited task required.

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDTO taskDTO = new TaskDTO(); // TaskDTO | Edited project
try {
    TaskDTO result = apiInstance.editTask(taskDTO);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#editTask");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskDTO** | [**TaskDTO**](TaskDTO.md)| Edited project |

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="editTask_0"></a>
# **editTask_0**
> TaskDTO editTask_0(taskDTO)

Edit task

Returns edited task. Edited task required.

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDTO taskDTO = new TaskDTO(); // TaskDTO | Edited project
try {
    TaskDTO result = apiInstance.editTask_0(taskDTO);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#editTask_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskDTO** | [**TaskDTO**](TaskDTO.md)| Edited project |

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAllProjects"></a>
# **getAllProjects**
> List&lt;ProjectDTO&gt; getAllProjects()

Get all projects

Returns a complete list of project details by order of creation.

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<ProjectDTO> result = apiInstance.getAllProjects();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getAllProjects");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;ProjectDTO&gt;**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAllTasks"></a>
# **getAllTasks**
> List&lt;TaskDTO&gt; getAllTasks()

Get all projects

Returns a complete list of projects details by order of creation.

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<TaskDTO> result = apiInstance.getAllTasks();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getAllTasks");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;TaskDTO&gt;**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getProjectById"></a>
# **getProjectById**
> ProjectDTO getProjectById(id)

Get project by ID

Returns project by unique ID. Unique ID required.

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "\"7ca5a0e6-5f22-4955-b134-42f826a2d8dc\""; // String | Unique ID of project
try {
    ProjectDTO result = apiInstance.getProjectById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getProjectById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Unique ID of project |

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getTaskById"></a>
# **getTaskById**
> TaskDTO getTaskById(id)

Get task by ID

Returns task by unique ID. Unique ID required.

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "\"7ca5a0e6-5f22-4955-b134-42f826a2d8dc\""; // String | Unique ID of task
try {
    TaskDTO result = apiInstance.getTaskById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getTaskById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Unique ID of task |

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

