<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<jsp:include page="../include/_header.jsp"/>

<div class="main_header-content">
    <sec:authorize access="isAuthenticated()">
        <h1>
            Welcome back, <b><sec:authentication property="name"/></b>, to Task Manager Application!
        </h1>
    </sec:authorize>

    <sec:authorize access="!isAuthenticated()">
        <h1>
            Welcome to Task Manager Application!
        </h1>
    </sec:authorize>
</div>

<jsp:include page="../include/_footer.jsp"/>