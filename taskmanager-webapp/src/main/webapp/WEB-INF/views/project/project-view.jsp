<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="../../include/_header.jsp" />

<div class="list_header-content">
    <h2>View project: &laquo;${project.title}&raquo;</h2>
</div>

<div class="main-content">

    <table>
        <tr>
            <th width="200" nowrap="nowrap">ID</th>
            <th width="200" nowrap="nowrap">TITLE</th>
            <th width="100%">DESCRIPTION</th>
            <th width="100%">STATUS</th>
            <th width="200">CREATION DATE</th>
            <th width="150">START DATE</th>
            <th width="150">END DATE</th>
        </tr>
            <tr>
                <td>
                    <c:out value="${project.id}" />
                </td>

                <td>
                    <c:out value="${project.title}" />
                </td>

                <td>
                    <c:out value="${project.description}" />
                </td>

                <td>
                    <c:out value="${project.status.title}" />
                </td>

                <td>
                    <fmt:formatDate value="${project.timeFrame.creationDate}" pattern="dd.MM.yyyy" />
                </td>

                <td>
                    <fmt:formatDate value="${project.timeFrame.startDate}" pattern="dd.MM.yyyy" />
                </td>

                <td>
                    <fmt:formatDate value="${project.timeFrame.endDate}" pattern="dd.MM.yyyy" />
                </td>
            </tr>
    </table>

    <div class="project_action-content" style="display: inline-block; float: right;">
        <a href="/project/edit/${project.id}"><button class="btn btn-outline-primary btn-sm">Edit project</button></a>
        <a href="/project/delete/${project.id}"><button class="btn btn-outline-danger btn-sm">Delete project</button></a>
    </div>

</div>

<jsp:include page="../../include/_footer.jsp" />