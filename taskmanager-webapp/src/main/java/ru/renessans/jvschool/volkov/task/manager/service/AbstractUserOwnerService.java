package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.api.service.IOwnerUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidValueException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractUserOwner;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.IOwnerUserRepository;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Transactional
public abstract class AbstractUserOwnerService<E extends AbstractUserOwner> extends AbstractService<E> implements IOwnerUserService<E> {

    @NotNull
    private final IOwnerUserRepository<E> ownerUserRepository;

    @NotNull
    private final IUserService userService;

    protected AbstractUserOwnerService(
            @NotNull final IOwnerUserRepository<E> ownerUserRepository,
            @NotNull final IUserService userService
    ) {
        super(ownerUserRepository);
        this.ownerUserRepository = ownerUserRepository;
        this.userService = userService;
    }

    @NotNull
    @SneakyThrows
    @Override
    public E addOwnerUser(
            @Nullable E value
    ) {
        if (Objects.isNull(value)) throw new InvalidValueException();
        @Nullable final User user = this.userService.getUserById(value.getUserId());
        value.setUser(user);
        return super.save(value);
    }

    @NotNull
    @SneakyThrows
    @Override
    public E updateOwnerUserByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String newTitle,
            @Nullable final String newDescription
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        if (StringUtils.isEmpty(newTitle)) throw new InvalidTitleException();
        if (StringUtils.isEmpty(newDescription)) throw new InvalidDescriptionException();

        @Nullable final E value = this.getOwnerUserByIndex(userId, index);
        if (Objects.isNull(value)) throw new InvalidOwnerUserException();
        value.setTitle(newTitle);
        value.setDescription(newDescription);

        return super.save(value);
    }

    @NotNull
    @SneakyThrows
    @Override
    public E updateOwnerUserById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String newTitle,
            @Nullable final String newDescription
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        if (StringUtils.isEmpty(id)) throw new InvalidIdException();
        if (StringUtils.isEmpty(newTitle)) throw new InvalidTitleException();
        if (StringUtils.isEmpty(newDescription)) throw new InvalidDescriptionException();

        @Nullable final E value = this.getOwnerUserById(userId, id);
        if (Objects.isNull(value)) throw new InvalidOwnerUserException();
        value.setTitle(newTitle);
        value.setDescription(newDescription);

        return super.save(value);
    }

    @SneakyThrows
    @Override
    public int deleteOwnerUserById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        if (StringUtils.isEmpty(id)) throw new InvalidIdException();
        @Nullable final E value = this.getOwnerUserById(userId, id);
        if (Objects.isNull(value)) return 0;
        return super.cascadeDeleteRecordById(value.getId());
    }

    @SneakyThrows
    @Override
    public int deleteOwnerUserByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        if (StringUtils.isEmpty(title)) throw new InvalidTitleException();
        @Nullable final E value = this.getOwnerUserByTitle(userId, title);
        if (Objects.isNull(value)) return 0;
        return super.cascadeDeleteRecordById(value.getId());
    }

    @SneakyThrows
    @Override
    public int deleteOwnerUserAll(
            @Nullable final String userId
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        @NotNull final Collection<E> values = this.getOwnerUserAll(userId);
        if (values.size() == 0) return 0;
        values.forEach(this.ownerUserRepository::delete);
        return 1;
    }

    @SneakyThrows
    @Override
    public int deleteOwnerUserByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        @NotNull final List<E> values = this.getOwnerUserAll(userId);
        if (index >= values.size()) return 0;
        @Nullable final E value = values.get(index);
        if (Objects.isNull(value)) return 0;
        return super.cascadeDeleteRecordById(value.getId());
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public E getOwnerUserByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty((index))) throw new IllegalIndexException();
        @NotNull final List<E> values = this.getOwnerUserAll(userId);
        if (index >= values.size()) return null;
        return values.get(index);
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public E getOwnerUserById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        if (StringUtils.isEmpty(id)) throw new InvalidIdException();
        return this.ownerUserRepository.getOwnerUserById(userId, id);
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public E getOwnerUserByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        if (StringUtils.isEmpty(title)) throw new InvalidTitleException();
        return this.ownerUserRepository.getOwnerUserByTitle(userId, title);
    }

    @NotNull
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public List<E> getOwnerUserAll(
            @Nullable final String userId
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        return this.ownerUserRepository.getAllOwnerUser(userId);
    }

    @NotNull
    @Transactional(readOnly = true)
    @Override
    public List<E> exportOwnerUser() {
        return this.ownerUserRepository.exportOwnerUser();
    }

    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public long countOwnerUser(
            @Nullable final String userId
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        return this.ownerUserRepository.countOwnerUser(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> initialOwnerUser(
            @Nullable final Collection<User> users
    ) {
        if (Objects.isNull(users)) throw new InvalidUserException();

        @NotNull final List<E> demoData = new ArrayList<>();
        users.forEach(user -> {
            final long countUserOwner = this.countOwnerUser(user.getId());
            if (ValidRuleUtil.isNullOrEmpty(countUserOwner)) {
                @NotNull final E data = addOwnerUser(
                        user.getId(), DemoDataConst.DEMO_TITLE_2ND, DemoDataConst.DEMO_DESCRIPTION_1ST
                );
                demoData.add(data);
            }
        });

        return demoData;
    }

    @PostConstruct
    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> initialOwnerUser() {
        @NotNull final List<E> demoData = new ArrayList<>();

        DemoDataConst.USERS_LOGINS.forEach(login -> {
            @Nullable final User user = this.userService.getUserByLogin(login);
            if (Objects.isNull(user)) return;
            final long countUserOwner = this.countOwnerUser(user.getId());
            if (ValidRuleUtil.isNullOrEmpty(countUserOwner)) {
                @NotNull final E data = addOwnerUser(
                        user.getId(), DemoDataConst.DEMO_TITLE_1ST,
                        DemoDataConst.DEMO_DESCRIPTION_2ND
                );
                demoData.add(data);
            }
        });

        return demoData;
    }

}