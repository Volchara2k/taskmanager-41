package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Collection;
import java.util.List;

public interface IOwnerUserService<E extends AbstractModel> extends IService<E> {

    @NotNull
    E addOwnerUser(
            @Nullable E value
    );

    @NotNull
    E addOwnerUser(
            @Nullable String userId,
            @Nullable String title,
            @Nullable String description
    );

    @NotNull
    E updateOwnerUserByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String newTitle,
            @Nullable String newDescription
    );

    @NotNull
    E updateOwnerUserById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String newTitle,
            @Nullable String newDescription
    );

    int deleteOwnerUserByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    int deleteOwnerUserById(
            @Nullable String userId,
            @Nullable String id
    );

    int deleteOwnerUserByTitle(
            @Nullable String userId,
            @Nullable String title
    );

    int deleteOwnerUserAll(
            @Nullable String userId
    );

    @Nullable
    E getOwnerUserByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @Nullable
    E getOwnerUserById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    E getOwnerUserByTitle(
            @Nullable String userId,
            @Nullable String title
    );

    @NotNull
    Collection<E> getOwnerUserAll(
            @Nullable String userId
    );

    @NotNull
    List<E> exportOwnerUser();

    long countOwnerUser(
            @Nullable String userId
    );

    @NotNull
    Collection<E> initialOwnerUser(
            @Nullable Collection<User> users
    );

    Collection<E> initialOwnerUser();

}