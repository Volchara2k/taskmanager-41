package ru.renessans.jvschool.volkov.task.manager.constant;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Arrays;
import java.util.Collection;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DemoDataConst {

    @NotNull
    public static final String DEMO_TITLE_1ST = "exceptions change";

    @NotNull
    public static final String DEMO_DESCRIPTION_1ST = "change the approach to exceptions";

    @NotNull
    public static final String DEMO_TITLE_2ND = "rethink view";

    @NotNull
    public static final String DEMO_DESCRIPTION_2ND = "rethink the view approach";

    @NotNull
    public static final String USER_TEST_LOGIN = "test";

    @NotNull
    public static final String USER_TEST_PASSWORD = "test";

    @NotNull
    public static final String USER_DEFAULT_LOGIN = "user";

    @NotNull
    public static final String USER_DEFAULT_PASSWORD = "user";

    @NotNull
    public static final String USER_ADMIN_LOGIN = "admin";

    @NotNull
    public static final String USER_ADMIN_PASSWORD = "admin";

    @NotNull
    public static final String USER_MANAGER_LOGIN = "manager";

    @NotNull
    public static final String USER_MANAGER_PASSWORD = "manager";

    @NotNull
    public static final Collection<User> USERS_WITH_UNSECURED_PASSWORD = Arrays.asList(
            new User(
                    DemoDataConst.USER_TEST_LOGIN,
                    DemoDataConst.USER_TEST_PASSWORD
            ),
            new User(
                    DemoDataConst.USER_ADMIN_LOGIN,
                    DemoDataConst.USER_ADMIN_PASSWORD,
                    UserRole.ADMIN
            ),
            new User(
                    DemoDataConst.USER_MANAGER_LOGIN,
                    DemoDataConst.USER_MANAGER_PASSWORD
            ),
            new User(
                    DemoDataConst.USER_DEFAULT_LOGIN,
                    DemoDataConst.USER_DEFAULT_PASSWORD
            )
    );

    @NotNull
    public static final Collection<String> USERS_LOGINS = Arrays.asList(
            DemoDataConst.USER_TEST_LOGIN,
            DemoDataConst.USER_ADMIN_LOGIN,
            DemoDataConst.USER_MANAGER_LOGIN,
            DemoDataConst.USER_DEFAULT_LOGIN
    );

}