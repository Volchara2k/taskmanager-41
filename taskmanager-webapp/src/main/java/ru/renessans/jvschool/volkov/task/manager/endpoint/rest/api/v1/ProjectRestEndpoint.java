package ru.renessans.jvschool.volkov.task.manager.endpoint.rest.api.v1;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.rest.IProjectRestEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.util.CurrentUserUtil;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(
        value = "/api/projects",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @NotNull
    private final IProjectUserService projectUserService;

    @NotNull
    private final IProjectAdapterService projectAdapterService;

    @NotNull
    @GetMapping
    @ApiOperation(
            value = "Get all projects",
            notes = "Returns a complete list of project details by order of creation."
    )
    @Override
    public Collection<ProjectDTO> getAllProjects() {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        return this.projectUserService.getOwnerUserAll(userid)
                .stream()
                .map(this.projectAdapterService::toDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    @SneakyThrows
    @GetMapping("/project/view/{id}")
    @ApiOperation(
            value = "Get project by ID",
            notes = "Returns project by unique ID. Unique ID required."
    )
    @Override
    public ProjectDTO getProjectById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of project",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull final String id
    ) {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        @Nullable final Project project = this.projectUserService.getOwnerUserById(userid, id);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        return this.projectAdapterService.toDTO(project);
    }

    @Nullable
    @RequestMapping(
            value = "/project/create",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(
            value = "Create project",
            notes = "Returns created project. Created project required."
    )
    @Override
    public ProjectDTO createProject(
            @ApiParam(
                    name = "projectDTO",
                    type = "ProjectDTO",
                    value = "Created project",
                    required = true
            )
            @RequestBody @NotNull final ProjectDTO projectDTO
    ) {
        @Nullable final Project project = this.projectAdapterService.toModel(projectDTO);
        if (Objects.isNull(project)) return null;
        @NotNull final String userid = CurrentUserUtil.getUserId();
        project.setUserId(userid);
        try {
            this.projectUserService.addOwnerUser(project);
        } catch (@NotNull final Exception exception) {
            return null;
        }
        return projectDTO;
    }

    @DeleteMapping("/project/delete/{id}")
    @ApiOperation(
            value = "Delete project by id",
            notes = "Returns integer deleted flag: 1 - true, 0 - false. Unique ID required."
    )
    @Override
    public int deleteProjectById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of project",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull final String id
    ) {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        return this.projectUserService.deleteOwnerUserById(userid, id);
    }

    @Nullable
    @SneakyThrows
    @RequestMapping(
            value = "/project/edit",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(
            value = "Edit project",
            notes = "Returns edited project. Edited project required."
    )
    @Override
    public ProjectDTO editProject(
            @ApiParam(
                    name = "projectDTO",
                    type = "ProjectDTO",
                    value = "Edited project",
                    required = true
            )
            @RequestBody @NotNull final ProjectDTO projectDTO
    ) {
        @Nullable final Project project = this.projectAdapterService.toModel(projectDTO);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        @NotNull final String userid = CurrentUserUtil.getUserId();
        if (StringUtils.isEmpty(project.getUserId())) project.setUserId(userid);
        try {
            this.projectUserService.addOwnerUser(project);
        } catch (@NotNull final Exception exception) {
            return null;
        }
        return projectDTO;
    }

}