package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.dto.SecureUserDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.*;
import ru.renessans.jvschool.volkov.task.manager.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Service
public final class UserDetailService extends AbstractService<User> implements UserDetailsService, IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final PasswordEncoder passwordEncoder;

    public UserDetailService(
            @NotNull final IUserRepository userRepository,
            @NotNull final PasswordEncoder passwordEncoder
    ) {
        super(userRepository);
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(
            @NonNull final String login
    ) throws UsernameNotFoundException {
        @Nullable final User user = this.userRepository.getUserByLogin(login);
        if (Objects.isNull(user)) throw new UsernameNotFoundException(login);
        return SecureUserDTO.detailBuilder()
                .userDetails(
                        SecureUserDTO.builder()
                                .username(user.getLogin())
                                .password(user.getPasswordHash())
                                .roles(user.getRole().toString())
                                .accountLocked(user.getLockdown())
                                .build()
                )
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .build();
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public User getUserById(
            @Nullable final String id
    ) {
        if (StringUtils.isEmpty(id)) throw new InvalidIdException();
        return super.getRecordById(id);
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public User getUserByLogin(
            @Nullable final String login
    ) {
        if (StringUtils.isEmpty(login)) throw new InvalidLoginException();
        return this.userRepository.getUserByLogin(login);
    }

    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public boolean existsUserByLogin(
            @Nullable final String login
    ) {
        if (StringUtils.isEmpty(login)) throw new InvalidLoginException();
        return this.userRepository.existsByLogin(login);
    }

    @Nullable
    @Transactional(readOnly = true)
    @Override
    public UserRole getUserRole(
            @Nullable final String userId
    ) {
        if (StringUtils.isEmpty(userId)) return null;
        @Nullable final User user = this.getUserById(userId);
        if (Objects.isNull(user)) return null;
        return user.getRole();
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (StringUtils.isEmpty(login)) throw new InvalidLoginException();
        if (StringUtils.isEmpty(password)) throw new InvalidPasswordException();
        @NotNull final String passwordHash = this.passwordEncoder.encode(password);
        @NotNull final User user = new User(login, passwordHash);
        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName
    ) {
        if (StringUtils.isEmpty(login)) throw new InvalidLoginException();
        if (StringUtils.isEmpty(password)) throw new InvalidPasswordException();
        if (StringUtils.isEmpty(firstName)) throw new InvalidFirstNameException();
        @NotNull final String passwordHash = this.passwordEncoder.encode(password);
        @NotNull final User user = new User(login, passwordHash, firstName);
        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final UserRole userRole
    ) {
        if (StringUtils.isEmpty(login)) throw new InvalidLoginException();
        if (StringUtils.isEmpty(password)) throw new InvalidPasswordException();
        if (Objects.isNull(userRole)) throw new InvalidUserRoleException();
        @NotNull final String passwordHash = this.passwordEncoder.encode(password);
        @NotNull final User user = new User(login, passwordHash, userRole);
        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User updateUserPasswordById(
            @Nullable final String id,
            @Nullable final String newPassword
    ) {
        if (StringUtils.isEmpty(id)) throw new InvalidUserIdException();
        if (StringUtils.isEmpty(newPassword)) throw new InvalidPasswordException();
        @NotNull final String passwordHash = this.passwordEncoder.encode(newPassword);

        @Nullable final User user = this.getUserById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        user.setPasswordHash(passwordHash);

        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User editUserProfileById(
            @Nullable final String id,
            @Nullable final String firstName
    ) {
        if (StringUtils.isEmpty(id)) throw new InvalidUserIdException();
        if (StringUtils.isEmpty(firstName)) throw new InvalidFirstNameException();
        @Nullable final User user = this.getUserById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        user.setFirstName(firstName);
        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User editUserProfileById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName
    ) {
        if (StringUtils.isEmpty(id)) throw new InvalidUserIdException();
        if (StringUtils.isEmpty(firstName)) throw new InvalidFirstNameException();
        if (StringUtils.isEmpty(lastName)) throw new InvalidLastNameException();

        @Nullable final User user = this.getUserById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        user.setFirstName(firstName);
        user.setLastName(lastName);

        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User lockUserByLogin(
            @Nullable final String login
    ) {
        if (StringUtils.isEmpty(login)) throw new InvalidLoginException();
        @Nullable final User user = this.getUserByLogin(login);
        if (Objects.isNull(user)) throw new InvalidUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException();
        user.setLockdown(true);
        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User unlockUserByLogin(
            @Nullable final String login
    ) {
        if (StringUtils.isEmpty(login)) throw new InvalidLoginException();
        @Nullable final User user = this.getUserByLogin(login);
        if (Objects.isNull(user)) throw new InvalidUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException();
        user.setLockdown(false);
        return super.save(user);
    }

    @SneakyThrows
    @Override
    public int deleteUserById(
            @Nullable final String id
    ) {
        if (StringUtils.isEmpty(id)) throw new InvalidUserIdException();
        return super.cascadeDeleteRecordById(id);
    }

    @SneakyThrows
    @Override
    public int deleteUserByLogin(
            @Nullable final String login
    ) {
        if (StringUtils.isEmpty(login)) throw new InvalidLoginException();
        @Nullable final User user = this.getUserByLogin(login);
        if (Objects.isNull(user)) return 0;
        return super.cascadeDeleteRecordById(user.getId());
    }

    @PostConstruct
    @NotNull
    @Override
    public Collection<User> initialDemoUsers() {
        @NotNull final Collection<User> nonRewritableResult = new ArrayList<>();

        DemoDataConst.USERS_WITH_UNSECURED_PASSWORD.forEach(user -> {
            @NotNull final String demoLogin = user.getLogin();
            boolean existsDemoByLogin = this.existsUserByLogin(demoLogin);
            if (!existsDemoByLogin) {
                this.setSecuredPassword(user);
                @NotNull final User addUser = super.save(user);
                nonRewritableResult.add(addUser);
            }
        });

        return nonRewritableResult;
    }

    private void setSecuredPassword(
            @NotNull final User userWithUnsecuredPassword
    ) {
        @NotNull final String unsecuredPassword = userWithUnsecuredPassword.getPasswordHash();
        @NotNull final String passwordHash = this.passwordEncoder.encode(unsecuredPassword);
        userWithUnsecuredPassword.setPasswordHash(passwordHash);
    }

}