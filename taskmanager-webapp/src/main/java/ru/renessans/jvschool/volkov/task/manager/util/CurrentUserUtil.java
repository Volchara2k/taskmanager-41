package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.renessans.jvschool.volkov.task.manager.dto.SecureUserDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.security.AccessFailureException;

@UtilityClass
public final class CurrentUserUtil {

    @NotNull
    @SneakyThrows
    public String getUserId() {
        @NotNull final SecureUserDTO userDTO = fetchCurrentUser();
        return userDTO.getId();
    }

    @NotNull
    @SneakyThrows
    public SecureUserDTO fetchCurrentUser() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final Object principal = authentication.getPrincipal();
        if (principal instanceof SecureUserDTO) return (SecureUserDTO) principal;
        throw new AccessFailureException("Необходимо авторизоваться!");
    }

}