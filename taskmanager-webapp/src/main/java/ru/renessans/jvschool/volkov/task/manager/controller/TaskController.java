package ru.renessans.jvschool.volkov.task.manager.controller;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.api.controller.ITaskController;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SecureUserDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.Status;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.model.Task;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class TaskController implements ITaskController {

    @NotNull
    private final ITaskUserService taskUserService;

    @NotNull
    private final IProjectUserService projectUserService;

    @NotNull
    private final ITaskAdapterService taskAdapterService;

    @NotNull
    private final IProjectAdapterService projectAdapterService;

    @NotNull
    @GetMapping("/tasks")
    @Override
    public ModelAndView index(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-list");
        @NotNull final Collection<Task> tasks = this.taskUserService.getOwnerUserAll(userDTO.getId());
        modelAndView.addObject("tasks", tasks);
        return modelAndView;
    }

    @NotNull
    @GetMapping("/task/create")
    @Override
    public ModelAndView create(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-settable");
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(userDTO.getId());
        modelAndView.addObject("task", taskDTO);
        modelAndView.addObject("statuses", Status.values());
        @NotNull final Collection<ProjectDTO> projects =
                this.projectUserService.getOwnerUserAll(userDTO.getId())
                        .stream()
                        .map(this.projectAdapterService::toDTO)
                        .collect(Collectors.toList());
        modelAndView.addObject("projects", projects);
        return modelAndView;
    }

    @NotNull
    @PostMapping("/task/create")
    @Override
    public ModelAndView create(
            @ModelAttribute("task") @NotNull final TaskDTO taskDTO,
            @NotNull final BindingResult result
    ) {
        @Nullable final Task task = this.taskAdapterService.toModel(taskDTO);
        this.taskUserService.addOwnerUser(task);
        return new ModelAndView("redirect:/tasks");
    }

    @NotNull
    @SneakyThrows
    @GetMapping("/task/view/{id}")
    @Override
    public ModelAndView view(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Task task = this.taskUserService.getOwnerUserById(userDTO.getId(), id);
        if (Objects.isNull(task)) throw new InvalidOwnerUserException();
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-view");
        modelAndView.addObject("task", task);
        return modelAndView;
    }

    @Override
    @NotNull
    @GetMapping("/task/delete/{id}")
    public ModelAndView delete(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        this.taskUserService.deleteOwnerUserById(userDTO.getId(), id);
        return new ModelAndView("redirect:/tasks");
    }

    @NotNull
    @SneakyThrows
    @GetMapping("/task/edit/{id}")
    @Override
    public ModelAndView edit(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Task task = this.taskUserService.getOwnerUserById(userDTO.getId(), id);
        if (Objects.isNull(task)) throw new InvalidOwnerUserException();
        @Nullable final TaskDTO taskDTO = this.taskAdapterService.toDTO(task);
        if (Objects.isNull(taskDTO)) throw new InvalidOwnerUserException();

        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-settable");
        modelAndView.addObject("task", taskDTO);
        modelAndView.addObject("statuses", Status.values());
        @NotNull final Collection<ProjectDTO> projects =
                this.projectUserService.getOwnerUserAll(userDTO.getId())
                        .stream()
                        .map(this.projectAdapterService::toDTO)
                        .collect(Collectors.toList());
        modelAndView.addObject("projects", projects);

        return modelAndView;
    }

    @NotNull
    @SneakyThrows
    @PostMapping("/task/edit/{id}")
    @Override
    public ModelAndView edit(
            @ModelAttribute("task") @NotNull final TaskDTO taskDTO,
            @NotNull final BindingResult result
    ) {
        @Nullable final Task task = this.taskAdapterService.toModel(taskDTO);
        if (Objects.isNull(task)) throw new InvalidOwnerUserException();
        this.taskUserService.addOwnerUser(task);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/task/view/{id}");
        modelAndView.addObject("id", task.getId());
        return modelAndView;
    }

}