package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import java.util.Collection;

@SuppressWarnings("unused")
public interface ITaskSoapEndpoint {

    @WebMethod
    @WebResult(name = "taskDTO", partName = "taskDTO")
    @Nullable
    TaskDTO addTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @NotNull TaskDTO taskDTO
    );

    @WebMethod
    @WebResult(name = "taskDTO", partName = "taskDTO")
    @Nullable
    TaskDTO updateTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @NotNull TaskDTO taskDTO
    );

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    int deleteTaskById(
            @WebParam(name = "id", partName = "id") @Nullable String id
    );

    @WebMethod
    @WebResult(name = "taskDTO", partName = "taskDTO")
    @Nullable
    TaskDTO getTaskById(
            @WebParam(name = "id", partName = "id") @Nullable String id
    );

    @WebMethod
    @WebResult(name = "tasksDTO", partName = "tasksDTO")
    @NotNull
    Collection<TaskDTO> getAllTasks();

}