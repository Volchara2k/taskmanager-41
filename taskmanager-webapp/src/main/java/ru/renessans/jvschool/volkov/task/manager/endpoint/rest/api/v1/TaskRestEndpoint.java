package ru.renessans.jvschool.volkov.task.manager.endpoint.rest.api.v1;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.rest.ITaskRestEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.util.CurrentUserUtil;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(
        value = "/api/tasks",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @NotNull
    private final ITaskUserService taskUserService;

    @NotNull
    private final ITaskAdapterService taskAdapterService;

    @NotNull
    @GetMapping
    @ApiOperation(
            value = "Get all projects",
            notes = "Returns a complete list of projects details by order of creation."
    )
    @Override
    public Collection<TaskDTO> getAllTasks() {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        return this.taskUserService.getOwnerUserAll(userid)
                .stream()
                .map(this.taskAdapterService::toDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    @SneakyThrows
    @GetMapping("/task/view/{id}")
    @ApiOperation(
            value = "Get task by ID",
            notes = "Returns task by unique ID. Unique ID required."
    )
    @Override
    public TaskDTO getTaskById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of task",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull final String id
    ) {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        @Nullable final Task task = this.taskUserService.getOwnerUserById(userid, id);
        if (Objects.isNull(task)) throw new InvalidOwnerUserException();
        return this.taskAdapterService.toDTO(task);
    }

    @Nullable
    @RequestMapping(
            value = "/task/create",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(
            value = "Create task",
            notes = "Returns created task. Created task required."
    )
    @Override
    public TaskDTO createTask(
            @ApiParam(
                    name = "taskDTO",
                    type = "TaskDTO",
                    value = "Created project",
                    required = true
            )
            @RequestBody @NotNull final TaskDTO taskDTO
    ) {
        @Nullable final Task task = this.taskAdapterService.toModel(taskDTO);
        if (Objects.isNull(task)) return null;
        @NotNull final String userid = CurrentUserUtil.getUserId();
        task.setUserId(userid);
        try {
            this.taskUserService.addOwnerUser(task);
        } catch (@NotNull final Exception exception) {
            return null;
        }
        return taskDTO;
    }

    @DeleteMapping("/task/delete/{id}")
    @ApiOperation(
            value = "Delete task by id",
            notes = "Returns integer deleted flag: 1 - true, 0 - false. Unique ID required."
    )
    @Override
    public int deleteTaskById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of task",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull final String id
    ) {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        return this.taskUserService.deleteOwnerUserById(userid, id);
    }

    @Nullable
    @SneakyThrows
    @RequestMapping(
            value = "/task/edit",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(
            value = "Edit task",
            notes = "Returns edited task. Edited task required."
    )
    @Override
    public TaskDTO editTask(
            @ApiParam(
                    name = "taskDTO",
                    type = "TaskDTO",
                    value = "Edited project",
                    required = true
            )
            @RequestBody @NotNull final TaskDTO taskDTO
    ) {
        @Nullable final Task task = this.taskAdapterService.toModel(taskDTO);
        if (Objects.isNull(task)) throw new InvalidOwnerUserException();
        @NotNull final String userid = CurrentUserUtil.getUserId();
        if (StringUtils.isEmpty(task.getUserId())) task.setUserId(userid);
        try {
            this.taskUserService.addOwnerUser(task);
        } catch (@NotNull final Exception exception) {
            return null;
        }
        return taskDTO;
    }

}