package ru.renessans.jvschool.volkov.task.manager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@Table(name = "tm_user")
public final class User extends AbstractModel {

    @NotNull
    @Column(
            unique = true,
            nullable = false
    )
    private String login = "";

    @NotNull
    @Column(nullable = false)
    private String passwordHash = "";

    @Nullable
    @Column
    private String firstName = "";

    @Nullable
    @Column
    private String lastName = "";

    @Nullable
    @Column
    private String middleName = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRole role = UserRole.USER;

    @NotNull
    @Column(nullable = false)
    private Boolean lockdown = false;

    @Nullable
    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Project> projects = new ArrayList<>();

    @Nullable
    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

    public User(
            @NotNull final String login,
            @NotNull final String password
    ) {
        setLogin(login);
        setPasswordHash(password);
    }

    public User(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final UserRole role
    ) {
        setLogin(login);
        setPasswordHash(password);
        setRole(role);
    }

    public User(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String firstName
    ) {
        setLogin(login);
        setPasswordHash(password);
        setFirstName(firstName);
    }

}