package ru.renessans.jvschool.volkov.task.manager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class ApplicationVersionListener extends AbstractListener {

    @NotNull
    private static final String CMD_VERSION = "version";

    @NotNull
    private static final String ARG_VERSION = "-v";

    @NotNull
    private static final String DESC_VERSION = "вывод версии программы";

    @NotNull
    private static final String NOTIFY_VERSION = "Версия: 1.0.41.";

    @NotNull
    @Override
    public String command() {
        return CMD_VERSION;
    }

    @NotNull
    @Override
    public String argument() {
        return ARG_VERSION;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_VERSION;
    }

    @Async
    @Override
    @EventListener(condition = "@applicationVersionListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_VERSION);
    }

}