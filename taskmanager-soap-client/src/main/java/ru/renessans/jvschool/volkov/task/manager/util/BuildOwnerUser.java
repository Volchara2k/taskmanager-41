package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.*;

@UtilityClass
public class BuildOwnerUser {

    @NotNull
    public TaskDTO buildTaskDTO(
            @NotNull final String title,
            @NotNull final String description
    ) {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setTitle(title);
        taskDTO.setDescription(description);
        taskDTO.setStatus(Status.COMPLETED);
        @NotNull final TimeFrameDTO timeFrameDTO = new TimeFrameDTO();
        taskDTO.setTimeFrame(timeFrameDTO);
        return taskDTO;
    }

    @NotNull
    public TaskDTO buildTaskDTO(
            @NotNull final TaskDTO taskDTO,
            @NotNull final String title,
            @NotNull final String description
    ) {
        taskDTO.setTitle(title);
        taskDTO.setDescription(description);
        return taskDTO;
    }

    @NotNull
    public ProjectDTO buildProjectDTO(
            @NotNull final String title,
            @NotNull final String description
    ) {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setTitle(title);
        projectDTO.setDescription(description);
        projectDTO.setStatus(Status.COMPLETED);
        @NotNull final TimeFrameDTO timeFrameDTO = new TimeFrameDTO();
        projectDTO.setTimeFrame(timeFrameDTO);
        return projectDTO;
    }

    @NotNull
    public ProjectDTO buildProjectDTO(
            @NotNull final ProjectDTO projectDTO,
            @NotNull final String title,
            @NotNull final String description
    ) {
        projectDTO.setTitle(title);
        projectDTO.setDescription(description);
        return projectDTO;
    }

}