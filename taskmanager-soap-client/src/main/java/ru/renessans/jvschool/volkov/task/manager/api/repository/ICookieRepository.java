package ru.renessans.jvschool.volkov.task.manager.api.repository;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ICookieRepository {

    @NotNull
    List<String> getCookieHeaders();

    void setCookieHeaders(@NotNull List<String> cookieHeaders);

    void clearCookieHeaders();

}