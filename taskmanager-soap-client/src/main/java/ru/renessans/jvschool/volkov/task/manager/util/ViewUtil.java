package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.AbstractDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalProcessCompleting;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidAbstractModelException;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@UtilityClass
public final class ViewUtil {

    @NotNull
    private static final String SUCCESSFUL_RESULT = "Операция завершилась успешно!\n";

    @NotNull
    private static final String UNSUCCESSFUL_RESULT = "Операция завершилась неуспешно!\n";

    public void print(@NotNull final String aString) {
        System.out.println(aString);
    }

    @SneakyThrows
    public void print(final boolean aBoolean) {
        if (!aBoolean) {
            print(UNSUCCESSFUL_RESULT);
            throw new IllegalProcessCompleting();
        }
        print(SUCCESSFUL_RESULT);
    }

    @SneakyThrows
    public void print(final int aInt) {
        if (aInt == 0) {
            print(UNSUCCESSFUL_RESULT);
            throw new IllegalProcessCompleting();
        }
        print(SUCCESSFUL_RESULT);
    }

    public void print(@Nullable final Collection<?> aCollection) {
        if (ValidRuleUtil.isNullOrEmpty(aCollection)) {
            print("Список на текущий момент пуст.");
            return;
        }

        @NotNull final AtomicInteger index = new AtomicInteger(1);
        aCollection.forEach(element -> {
            print(index + ". " + element);
            index.getAndIncrement();
        });
        print(SUCCESSFUL_RESULT);
    }

    @SneakyThrows
    public void print(@Nullable final AbstractDTO aModelDTO) {
        if (Objects.isNull(aModelDTO)) {
            print(UNSUCCESSFUL_RESULT);
            throw new InvalidAbstractModelException();
        }
        print(aModelDTO.toString());
    }

    @NotNull
    public String getLine() {
        System.out.print("Введите строковые данные: ");
        return ScannerUtil.getLine();
    }

    @NotNull
    public Integer getInteger() {
        System.out.print("Введите индекс: ");
        return ScannerUtil.getInteger();
    }

}