package ru.renessans.jvschool.volkov.task.manager.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICookieService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.ProjectSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.BuildOwnerUser;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import javax.xml.ws.Holder;

@Component
public class ProjectCreateListener extends AbstractProjectListener {

    @NotNull
    private static final String CMD_PROJECT_CREATE = "project-create";

    @NotNull
    private static final String DESC_PROJECT_CREATE = "добавить новый проект";

    @NotNull
    private static final String NOTIFY_PROJECT_CREATE =
            "Происходит попытка инициализации создания проекта. \n" +
                    "Для создания проекта введите его заголовок и описание. ";

    public ProjectCreateListener(
            @NotNull final ProjectSoapEndpoint projectEndpoint,
            @NotNull final ICookieService sessionService
    ) {
        super(projectEndpoint, sessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_PROJECT_CREATE;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_PROJECT_CREATE;
    }

    @Async
    @Override
    @SneakyThrows
    @EventListener(condition = "@projectCreateListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_PROJECT_CREATE);
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();
        @NotNull final ProjectDTO projectDTO = BuildOwnerUser.buildProjectDTO(title, description);
        @NotNull final Holder<ProjectDTO> holder = new Holder<>(projectDTO);
        super.sessionService.setListCookieRowRequest(super.projectEndpoint);
        super.projectEndpoint.addProject(holder);
    }

}