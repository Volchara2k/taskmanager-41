package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandService {

    @Nullable
    Collection<String> getAllCommands();

    @Nullable
    Collection<String> getAllTerminalCommands();

    @Nullable
    Collection<String> getAllArgumentCommands();

}