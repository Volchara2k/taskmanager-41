package ru.renessans.jvschool.volkov.task.manager.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICookieService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.ProjectSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.BuildOwnerUser;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import javax.xml.ws.Holder;

@Component
public class ProjectUpdateListener extends AbstractProjectListener {

    @NotNull
    private static final String CMD_PROJECT_UPDATE_BY_ID = "project-update-by-id";

    @NotNull
    private static final String DESC_PROJECT_UPDATE_BY_ID = "обновить проект по идентификатору";

    @NotNull
    private static final String NOTIFY_PROJECT_UPDATE_BY_ID =
            "Происходит попытка инициализации обновления проекта. \n" +
                    "Для обновления проекта по идентификатору введите идентификатор проекта из списка. \n " +
                    "Для обновления проекта введите его заголовок или заголовок с описанием. ";

    public ProjectUpdateListener(
            @NotNull final ProjectSoapEndpoint projectEndpoint,
            @NotNull final ICookieService sessionService
    ) {
        super(projectEndpoint, sessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_PROJECT_UPDATE_BY_ID;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_PROJECT_UPDATE_BY_ID;
    }

    @Async
    @Override
    @SneakyThrows
    @EventListener(condition = "@projectUpdateListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_PROJECT_UPDATE_BY_ID);
        @NotNull final String id = ViewUtil.getLine();
        super.sessionService.setListCookieRowRequest(super.projectEndpoint);
        @NotNull final ProjectDTO projectResponse = super.projectEndpoint.getProjectById(id);

        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();
        @NotNull final ProjectDTO updateProjectDTO =
                BuildOwnerUser.buildProjectDTO(projectResponse, title, description);

        @NotNull final Holder<ProjectDTO> holder = new Holder<>(updateProjectDTO);
        super.projectEndpoint.updateProject(holder);
    }

}