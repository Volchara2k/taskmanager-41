package ru.renessans.jvschool.volkov.task.manager.api.configuration;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.*;

import java.util.concurrent.Executor;

public interface IEndpointConfiguration {

    @NotNull
    Executor executor();

    @NotNull
    AuthenticationSoapEndpointService authenticationEndpointService();

    @NotNull
    ProjectSoapEndpointService projectEndpointService();

    @NotNull
    TaskSoapEndpointService taskEndpointService();

    @NotNull
    AuthenticationSoapEndpoint authenticationEndpoint(
            @NotNull AuthenticationSoapEndpointService authenticationSoapEndpointService
    );

    @NotNull
    ProjectSoapEndpoint projectEndpoint(
            @NotNull ProjectSoapEndpointService projectEndpointService
    );

    @NotNull
    TaskSoapEndpoint taskEndpoint(
            @NotNull TaskSoapEndpointService taskEndpointService
    );

}