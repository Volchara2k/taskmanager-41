#!/usr/bin/env bash

echo "Attempting to start the task server..."

PORT="8080"
if [ -n "$1" ]; then
  PORT=$1;
fi
FILE_PID="$PORT.pid"

echo "Starting server at $PORT..."
if [ -f ./"$FILE_PID" ]; then
	echo "Task manager server already started!"
	exit 1;
fi

mkdir -p ../bash/logs
rm -f ../bash/logs/taskmanager-server.log
nohup java -Dport="$PORT" -jar ../../taskmanager-server.jar > ../bash/logs/taskmanager-server.log 2>&1 &
echo $! > ./"$FILE_PID"
echo "Task manager server is running with pid "$!