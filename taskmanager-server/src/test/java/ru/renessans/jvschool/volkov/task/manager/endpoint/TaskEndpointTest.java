package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;

@RunWith(value = JUnitParamsRunner.class)
@Category({PositiveImplementation.class, EndpointImplementation.class})
public final class TaskEndpointTest {

//    @NotNull
//    private final ITaskEndpoint taskEndpoint = new TaskEndpoint();

//    @Before
//    public void loadConfigurationBefore() {
//        Assert.assertNotNull(this.configService);
//        this.configService.load();
//        this.serviceLocator.getEntityManagerService().build();
//    }
//
//    @Test
//    @TestCaseName("Run testAddTask for addTask(session, {1})")
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testAddTask(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.taskEndpoint);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final TaskDTO addTask = this.taskEndpoint.addTask(
//                conversion, task.getTitle(), task.getDescription()
//        );
//        Assert.assertNotNull(addTask);
//        Assert.assertEquals(addRecord.getId(), addTask.getUserId());
//        Assert.assertEquals(task.getTitle(), addTask.getTitle());
//        Assert.assertEquals(task.getDescription(), addTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testUpdateTaskByIndex for updateTaskByIndex(session, {1})")
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testUpdateTaskByIndex(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.taskEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Task addTaskRecord = this.taskService.add(
//                addRecord.getId(), task.getTitle(), task.getDescription()
//        );
//        Assert.assertNotNull(addTaskRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final String newDescription = UUID.randomUUID().toString();
//        Assert.assertNotNull(newDescription);
//        task.setTitle(newDescription);
//        @Nullable final TaskDTO updateTask =
//                this.taskEndpoint.updateTaskByIndex(conversion, 0, task.getTitle(), newDescription);
//        Assert.assertNotNull(updateTask);
//        Assert.assertEquals(addTaskRecord.getId(), updateTask.getId());
//        Assert.assertEquals(addTaskRecord.getUserId(), updateTask.getUserId());
//        Assert.assertEquals(addTaskRecord.getTitle(), updateTask.getTitle());
//        Assert.assertEquals(addTaskRecord.getDescription(), updateTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testUpdateTaskById for updateTaskById(session, {1})")
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testUpdateTaskById(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.taskEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Task addTaskRecord = this.taskService.add(
//                addRecord.getId(), task.getTitle(), task.getDescription()
//        );
//        Assert.assertNotNull(addTaskRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final String newDescription = UUID.randomUUID().toString();
//        Assert.assertNotNull(newDescription);
//        @Nullable final TaskDTO updateTask = this.taskEndpoint.updateTaskById(
//                conversion, addTaskRecord.getId(), task.getTitle(), newDescription
//        );
//        Assert.assertNotNull(updateTask);
//        Assert.assertEquals(addTaskRecord.getId(), updateTask.getId());
//        Assert.assertEquals(addTaskRecord.getUserId(), updateTask.getUserId());
//        Assert.assertEquals(addTaskRecord.getTitle(), updateTask.getTitle());
//        Assert.assertEquals(addTaskRecord.getDescription(), updateTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteTaskById for deleteTaskById(session, {1})")
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testDeleteTaskById(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.taskEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Task addTaskRecord = this.taskService.add(
//                addRecord.getId(), task.getTitle(), task.getDescription()
//        );
//        Assert.assertNotNull(addTaskRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final TaskDTO deleteTask = this.taskEndpoint.deleteTaskById(conversion, addTaskRecord.getId());
//        Assert.assertNotNull(deleteTask);
//        Assert.assertEquals(addTaskRecord.getId(), deleteTask.getId());
//        Assert.assertEquals(addTaskRecord.getUserId(), deleteTask.getUserId());
//        Assert.assertEquals(addTaskRecord.getTitle(), deleteTask.getTitle());
//        Assert.assertEquals(addTaskRecord.getDescription(), deleteTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteTaskByIndex for deleteTaskByIndex(session, 0)")
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testDeleteTaskByIndex(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.taskEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Task addTaskRecord = this.taskService.add(
//                addRecord.getId(), task.getTitle(), task.getDescription()
//        );
//        Assert.assertNotNull(addTaskRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final TaskDTO deleteTask = this.taskEndpoint.deleteTaskByIndex(conversion, 0);
//        Assert.assertNotNull(deleteTask);
//        Assert.assertEquals(addTaskRecord.getId(), deleteTask.getId());
//        Assert.assertEquals(addTaskRecord.getUserId(), deleteTask.getUserId());
//        Assert.assertEquals(addTaskRecord.getTitle(), deleteTask.getTitle());
//        Assert.assertEquals(addTaskRecord.getDescription(), deleteTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteTaskByTitle for deleteTaskByTitle(session, {1})")
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testDeleteTaskByTitle(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.taskEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Task addTaskRecord = this.taskService.add(
//                addRecord.getId(), task.getTitle(), task.getDescription()
//        );
//        Assert.assertNotNull(addTaskRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final TaskDTO deleteTask = this.taskEndpoint.deleteTaskByTitle(conversion, addTaskRecord.getTitle());
//        Assert.assertNotNull(deleteTask);
//        Assert.assertEquals(addTaskRecord.getId(), deleteTask.getId());
//        Assert.assertEquals(addTaskRecord.getUserId(), deleteTask.getUserId());
//        Assert.assertEquals(addTaskRecord.getTitle(), deleteTask.getTitle());
//        Assert.assertEquals(addTaskRecord.getDescription(), deleteTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteAllTasks for deleteAllTasks(session)")
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testDeleteAllTasks(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.taskEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Task addTaskRecord = this.taskService.add(
//                addRecord.getId(), task.getTitle(), task.getDescription()
//        );
//        Assert.assertNotNull(addTaskRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final Collection<TaskDTO> deleteTasks = this.taskEndpoint.deleteAllTasks(conversion);
//        Assert.assertNotNull(deleteTasks);
//        Assert.assertNotEquals(0, deleteTasks.size());
//        final boolean isUserTasks = deleteTasks.stream().allMatch(entity -> addRecord.getId().equals(entity.getUserId()));
//        Assert.assertTrue(isUserTasks);
//    }
//
//    @Test
//    @TestCaseName("Run testGetTaskById for getTaskById(session, {1})")
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testGetTaskById(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.taskEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Task addTaskRecord = this.taskService.add(
//                addRecord.getId(), task.getTitle(), task.getDescription()
//        );
//        Assert.assertNotNull(addTaskRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final TaskDTO getTask = this.taskEndpoint.getTaskById(conversion, addTaskRecord.getId());
//        Assert.assertNotNull(getTask);
//        Assert.assertEquals(addTaskRecord.getId(), getTask.getId());
//        Assert.assertEquals(addTaskRecord.getUserId(), getTask.getUserId());
//        Assert.assertEquals(addTaskRecord.getTitle(), getTask.getTitle());
//        Assert.assertEquals(addTaskRecord.getDescription(), getTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testGetTaskByIndex for getTaskByIndex(session, 0)")
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testGetTaskByIndex(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.taskEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Task addTaskRecord = this.taskService.add(
//                addRecord.getId(), task.getTitle(), task.getDescription()
//        );
//        Assert.assertNotNull(addTaskRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final TaskDTO getTask = this.taskEndpoint.getTaskByIndex(conversion, 0);
//        Assert.assertNotNull(getTask);
//        Assert.assertEquals(addTaskRecord.getId(), getTask.getId());
//        Assert.assertEquals(addTaskRecord.getUserId(), getTask.getUserId());
//        Assert.assertEquals(addTaskRecord.getTitle(), getTask.getTitle());
//        Assert.assertEquals(addTaskRecord.getDescription(), getTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testGetTaskByTitle for getTaskByTitle(session, {1})")
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testGetTaskByTitle(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.taskEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Task addTaskRecord = this.taskService.add(
//                addRecord.getId(), task.getTitle(), task.getDescription()
//        );
//        Assert.assertNotNull(addTaskRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final TaskDTO getTask = this.taskEndpoint.getTaskByTitle(conversion, addTaskRecord.getTitle());
//        Assert.assertNotNull(getTask);
//        Assert.assertEquals(addTaskRecord.getId(), getTask.getId());
//        Assert.assertEquals(addTaskRecord.getUserId(), getTask.getUserId());
//        Assert.assertEquals(addTaskRecord.getTitle(), getTask.getTitle());
//        Assert.assertEquals(addTaskRecord.getDescription(), getTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testGetAllTasks for getAllTasks(session)")
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testGetAllTasks(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.taskEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Task addTaskRecord = this.taskService.add(
//                addRecord.getId(), task.getTitle(), task.getDescription()
//        );
//        Assert.assertNotNull(addTaskRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final Collection<TaskDTO> getAllTasks = this.taskEndpoint.getAllTasks(conversion);
//        Assert.assertNotNull(getAllTasks);
//        Assert.assertNotEquals(0, getAllTasks.size());
//        final boolean isUserTasks = getAllTasks.stream().allMatch(entity -> addRecord.getId().equals(entity.getUserId()));
//        Assert.assertTrue(isUserTasks);
//    }

}