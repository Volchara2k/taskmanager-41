package ru.renessans.jvschool.volkov.task.manager.util;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataBaseProvider;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.file.InvalidPathnameException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.UtilityImplementation;

import java.io.File;

@RunWith(value = JUnitParamsRunner.class)
public final class FileUtilTest {

    @Rule
    @NotNull
    public TemporaryFolder file = new TemporaryFolder();

    @Test(expected = InvalidPathnameException.class)
    @TestCaseName("Run testNegativeCreateFileWithoutFilename for create(\"{0}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeCreateFileWithoutFilename(
            @Nullable final String filename
    ) {
        FileUtil.create(filename);
    }

    @Test(expected = InvalidPathnameException.class)
    @TestCaseName("Run testNegativeReadFileWithoutFilename for read(\"{0}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeReadFileWithoutFilename(
            @Nullable final String filename
    ) {
        FileUtil.read(filename);
    }

    @Test
    @TestCaseName("Run testNegativeDeleteFileWithoutFilename for delete(\"{0}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    @SneakyThrows
    public void testNegativeDeleteFileWithoutFilename(
            @Nullable final String filename
    ) {
        Assert.assertNotNull(this.file);
        @NotNull final File root = this.file.getRoot();
        Assert.assertNotNull(root);
        @NotNull final String rootPath = root.getAbsolutePath();
        Assert.assertNotNull(rootPath);
        @NotNull final String filepath = rootPath + File.separator + filename;
        Assert.assertNotNull(filepath);

        boolean isEmptyFilename = FileUtil.delete(filepath);
        Assert.assertFalse(isEmptyFilename);
    }

    @Test
    @TestCaseName("Run testNegativeDeleteFileWithoutFile for delete(\"{0}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testNegativeDeleteFileWithoutFile(
            @NotNull final String filename
    ) {
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File root = this.file.getRoot();
        Assert.assertNotNull(root);
        @NotNull final String rootPath = root.getAbsolutePath();
        Assert.assertNotNull(rootPath);
        @NotNull final String filepath = rootPath + File.separator + filename;
        Assert.assertNotNull(filepath);

        boolean isNotExistsFile = FileUtil.delete(filepath);
        Assert.assertFalse(isNotExistsFile);
    }

    @Test
    @TestCaseName("Run testNegativeIsNotExistsWithoutExistsFile for isNotExists(null)")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativeIsNotExistsWithoutExistsFile() {
        Assert.assertNotNull(this.file);
        @NotNull final File root = this.file.getRoot();
        Assert.assertNotNull(root);
        @NotNull final String rootPath = root.getAbsolutePath();
        Assert.assertNotNull(rootPath);
        @NotNull final String filepath = rootPath + File.separator + file;
        Assert.assertNotNull(filepath);

        boolean isNotExistsFile = FileUtil.isNotExists((@Nullable File) null);
        Assert.assertFalse(isNotExistsFile);
    }

    @Test
    @TestCaseName("Run testNegativeFileIsNotExistsWithoutFile for isNotExists(\"{0}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    @SneakyThrows
    public void testNegativeFileIsNotExistsWithoutFile(
            @NotNull final String filename
    ) {
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File file = this.file.newFolder(filename);
        Assert.assertNotNull(file);
        boolean isNotFile = FileUtil.isNotExists(file);
        Assert.assertTrue(isNotFile);
    }

    @Test
    @TestCaseName("Run testNegativeIsNotExistsWithoutFilename for isNotExists(\"{0}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeIsNotExistsWithoutFilename(
            @Nullable final String filename
    ) {
        boolean isNotExistsFilename = FileUtil.isNotExists(filename);
        Assert.assertFalse(isNotExistsFilename);
    }

    @Test
    @TestCaseName("Run testCreateFile for create(\"{0}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    @SneakyThrows
    public void testCreateFile(
            @NotNull final String filename
    ) {
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File file = this.file.newFolder();
        Assert.assertNotNull(file);
        @NotNull final String filepath = file + File.separator + filename;
        Assert.assertNotNull(filepath);

        @NotNull final File created = FileUtil.create(filepath);
        Assert.assertNotNull(created);
    }

    @Test
    @TestCaseName("Run testReadFile for read(\"{0}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    @SneakyThrows
    public void testReadFile(
            @NotNull final String filename
    ) {
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File file = this.file.newFile(filename);
        Assert.assertNotNull(file);
        @NotNull final String filepath = file.getAbsolutePath();
        Assert.assertNotNull(filepath);

        final byte[] read = FileUtil.read(filepath);
        Assert.assertNotNull(read);
    }

    @Test
    @TestCaseName("Run testDeleteFile for delete(\"{0}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    @SneakyThrows
    public void testDeleteFile(
            @NotNull final String filename
    ) {
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File file = this.file.newFile(filename);
        Assert.assertNotNull(file);
        @NotNull final String filepath = file.getAbsolutePath();
        Assert.assertNotNull(filepath);

        final boolean isDeleted = FileUtil.delete(filepath);
        Assert.assertTrue(isDeleted);
    }

    @Test
    @TestCaseName("Run testIsNotExistsFile for isNotExists(\"{0}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    @SneakyThrows
    public void testIsNotExistsFile(
            @NotNull final String filename
    ) {
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File file = this.file.newFolder(filename);
        Assert.assertNotNull(file);

        final boolean isNotExists = FileUtil.isNotExists(file);
        Assert.assertTrue(isNotExists);
    }

    @Test
    @TestCaseName("Run testIsNotExistsFilename for isNotExists(\"{0}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    @SneakyThrows
    public void testIsNotExistsFilename(
            @NotNull final String filename
    ) {
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File root = this.file.getRoot();
        Assert.assertNotNull(root);
        @NotNull final String rootPath = root.getAbsolutePath();
        Assert.assertNotNull(rootPath);
        @NotNull final String filepath = rootPath + File.separator + filename;
        Assert.assertNotNull(filepath);

        final boolean isNotExists = FileUtil.isNotExists(filepath);
        Assert.assertTrue(isNotExists);
    }

}