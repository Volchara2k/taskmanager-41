package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.JUnitParamsRunner;
import org.jetbrains.annotations.NotNull;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IPropertyService;
import ru.renessans.jvschool.volkov.task.manager.service.ConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.service.PropertyService;

@RunWith(value = JUnitParamsRunner.class)
public final class UserRepositoryTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConfigurationService CONFIG_SERVICE = new ConfigurationService(PROPERTY_SERVICE);

//    @Before
//    public void loadConfigurationBefore() {
//        Assert.assertNotNull(this.configService);
//        this.configService.load();
//        this.entityManagerFactoryService.build();
//    }
//
//    @Test(expected = InvalidUserException.class)
//    @TestCaseName("Run testNegativeDeleteByLogin for deleteByLogin({0})")
//    @Category({NegativeImplementation.class, RepositoryImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersCaseData"
//    )
//
//    public void testNegativeDeleteByLogin(
//            @NotNull final User user
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(this.entityManagerFactoryService);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
//        Assert.assertNotNull(userRepository);
//        userRepository.deleteByLogin(user.getLogin());
//    }
//
//    @Test
//    @TestCaseName("Run testGetByLogin for getByLogin({0})")
//    @Category({PositiveImplementation.class, RepositoryImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersCaseData"
//    )
//    public void testGetByLogin(
//            @NotNull final User user
//    ) {
//        Assert.assertNotNull(this.entityManagerFactoryService);
//        Assert.assertNotNull(user);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
//        Assert.assertNotNull(userRepository);
//        @NotNull final User addRecord = userRepository.persist(user);
//        Assert.assertNotNull(addRecord);
//
//        @Nullable final User getUser = userRepository.getByLogin(addRecord.getLogin());
//        Assert.assertNotNull(getUser);
//        Assert.assertEquals(user.getId(), getUser.getId());
//        Assert.assertEquals(user.getLogin(), getUser.getLogin());
//        Assert.assertEquals(user.getPasswordHash(), getUser.getPasswordHash());
//        Assert.assertEquals(user.getRole(), getUser.getRole());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteByLogin for deleteByLogin({0})")
//    @Category({PositiveImplementation.class, RepositoryImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersCaseData"
//    )
//    public void testDeleteByLogin(
//            @NotNull final User user
//    ) {
//        Assert.assertNotNull(this.entityManagerFactoryService);
//        Assert.assertNotNull(user);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
//        Assert.assertNotNull(userRepository);
//        @NotNull final User addRecord = userRepository.persist(user);
//        Assert.assertNotNull(addRecord);
//
//        @Nullable final User deleteUser = userRepository.deleteByLogin(user.getLogin());
//        Assert.assertNotNull(deleteUser);
//        Assert.assertEquals(user.getId(), deleteUser.getId());
//        Assert.assertEquals(user.getLogin(), deleteUser.getLogin());
//        Assert.assertEquals(user.getPasswordHash(), deleteUser.getPasswordHash());
//        Assert.assertEquals(user.getRole(), deleteUser.getRole());
//    }

}