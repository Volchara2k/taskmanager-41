package ru.renessans.jvschool.volkov.task.manager.casedata;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;

public final class CaseDataTaskProvider {

    @SuppressWarnings("unused")
    public Object[] validTasksCaseData() {
        @Nullable User user;
        return new Object[]{
                new Object[]{
                        user = new User(
                                "eZ7jautHKpMedsGGZjlN",
                                "LIH5rdyt0B95tXfujB8U"
                        ),
                        new Task(
                                user.getId(),
                                "TH9X0zwSJHwni4lCaKV9",
                                "ePZAE5VOzQBjos1hSx8u"
                        )
                },
                new Object[]{
                        user = new User(
                                "S9UwYwXLYr0mItuR62RT",
                                "JuRjPc9MaLyekq2wlUaI"
                        ),
                        new Task(
                                user.getId(),
                                "6GN4YiYEys13PThxXb10",
                                "My3ZR4o6SpFPYlNOtClS"
                        )
                },
                new Object[]{
                        user = new User(
                                "sPcoZWNQSd5MdNh2xets",
                                "fDp35xmsrKoLsq0kfG70",
                                UserRole.USER
                        ),
                        new Task(
                                user.getId(),
                                "M2W3rPizobp07UGwzlrg",
                                "mD5R1Sj0U5cz6ZxoGFOV"
                        )
                }
        };
    }

}