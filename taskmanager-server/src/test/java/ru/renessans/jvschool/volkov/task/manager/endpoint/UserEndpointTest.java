package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;

@RunWith(value = JUnitParamsRunner.class)
@Category({PositiveImplementation.class, EndpointImplementation.class})
public final class UserEndpointTest {

//    @NotNull
//    private final IUserEndpoint userEndpoint = new UserEndpoint();

//    @Before
//    public void loadConfigurationBefore() {
//        Assert.assertNotNull(this.configService);
//        this.configService.load();
//        this.serviceLocator.getEntityManagerService().build();
//    }
//
//    @Test
//    @TestCaseName("Run testGetUser for getUser(session)")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testGetUser(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addRecord = this.userService.addUser(login, password);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                login, password
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final UserDTO getUser = this.userEndpoint.getUser(conversion);
//        Assert.assertNotNull(getUser);
//        Assert.assertEquals(addRecord.getId(), getUser.getId());
//        Assert.assertEquals(login, getUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(addRecord.getRole(), getUser.getRole());
//        Assert.assertEquals(hashPassword, getUser.getPasswordHash());
//    }
//
//    @Test
//    @TestCaseName("Run testEditProfileById for editProfile(session, \"{2}\")")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsWithNewEntityCaseData"
//    )
//    public void testEditProfile(
//            @NotNull final String login,
//            @NotNull final String password,
//            @NotNull final String newFirstName
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        Assert.assertNotNull(newFirstName);
//        @NotNull final User addRecord = this.userService.addUser(login, password);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                login, password
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final UserDTO editUser = this.userEndpoint.editProfile(conversion, newFirstName);
//        Assert.assertNotNull(editUser);
//        Assert.assertEquals(addRecord.getId(), editUser.getId());
//        Assert.assertEquals(login, editUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(hashPassword, editUser.getPasswordHash());
//        Assert.assertEquals(addRecord.getRole(), editUser.getRole());
//        Assert.assertEquals(newFirstName, editUser.getFirstName());
//    }
//
//    @Test
//    @TestCaseName("Run testEditProfileWithLastName for editProfileWithLastName(session, \"{2}\", \"{2}\")")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsWithNewEntityCaseData"
//    )
//    public void testEditProfileWithLastName(
//            @NotNull final String login,
//            @NotNull final String password,
//            @NotNull final String newData
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        Assert.assertNotNull(newData);
//        @NotNull final User addRecord = this.userService.addUser(login, password);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                login, password
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final UserDTO editUser = this.userEndpoint.editProfileWithLastName(conversion, newData, newData);
//        Assert.assertNotNull(editUser);
//        Assert.assertEquals(addRecord.getId(), editUser.getId());
//        Assert.assertEquals(login, editUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(hashPassword, editUser.getPasswordHash());
//        Assert.assertEquals(addRecord.getRole(), editUser.getRole());
//        Assert.assertEquals(newData, editUser.getFirstName());
//        Assert.assertEquals(newData, editUser.getLastName());
//    }
//
//    @Test
//    @TestCaseName("Run testUpdatePassword for updatePassword(session, \"{2}\")")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsWithNewEntityCaseData"
//    )
//    public void testUpdatePassword(
//            @NotNull final String login,
//            @NotNull final String password,
//            @NotNull final String newPassword
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        Assert.assertNotNull(newPassword);
//        @NotNull final User addRecord = this.userService.addUser(login, password);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                login, password
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final UserDTO updatePassword = this.userEndpoint.updatePassword(conversion, newPassword);
//        Assert.assertNotNull(updatePassword);
//        Assert.assertEquals(addRecord.getId(), updatePassword.getId());
//        Assert.assertEquals(login, updatePassword.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(newPassword);
//        Assert.assertEquals(addRecord.getRole(), updatePassword.getRole());
//        Assert.assertEquals(hashPassword, updatePassword.getPasswordHash());
//    }

}