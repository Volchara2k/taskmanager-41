package ru.renessans.jvschool.volkov.task.manager.casedata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public final class CaseDataEndpointUtilProvider {

    @SuppressWarnings("unused")
    public Object[] invalidObjectsCaseData() {
        return new Object[]{
                new Object[]{
                        null,
                        "127.0.0.1",
                        9112
                },
                new Object[]{
                        Arrays.asList(
                                null,
                                null,
                                null
                        ),
                        "127.0.0.1",
                        9111
                },
                new Object[]{
                        Collections.emptyList(),
                        "127.0.0.1",
                        9110
                },
                new Object[]{
                        new ArrayList<>(),
                        "127.0.0.1",
                        9109
                }
        };
    }

//    @SuppressWarnings("unused")
//    public Object[] invalidHostsCaseData() {
//        return new Object[]{
//                new Object[]{new SessionEndpoint(), null, 9115},
//                new Object[]{new UserEndpoint(), "", 9114},
//                new Object[]{new AdminEndpoint(), "    ", 9113}
//        };
//    }

//    @SuppressWarnings("unused")
//    public Object[] invalidHostsObjectsCaseData() {
//        return new Object[]{
//                new Object[]{
//                        Arrays.asList(
//                                new SessionEndpoint(),
//                                new UserEndpoint(),
//                                null
//                        ),
//                        null,
//                        9118
//                },
//                new Object[]{
//                        Collections.singletonList(
//                                new SessionEndpoint()
//                        ),
//                        "",
//                        9117
//                },
//                new Object[]{
//                        Arrays.asList(
//                                null,
//                                new UserEndpoint(),
//                                null
//                        ),
//                        "    ",
//                        9116
//                }
//        };
//    }

//    @SuppressWarnings("unused")
//    public Object[] invalidPortsCaseData() {
//        return new Object[]{
//                new Object[]{new SessionEndpoint(), "127.0.0.1", null},
//                new Object[]{new UserEndpoint(), "127.0.0.1", -1},
//                new Object[]{new AdminEndpoint(), "127.0.0.1", -10}
//        };
//    }

//    @SuppressWarnings("unused")
//    public Object[] invalidPortsObjectsCaseData() {
//        return new Object[]{
//                new Object[]{
//                        Arrays.asList(
//                                new SessionEndpoint(),
//                                new UserEndpoint(),
//                                null
//                        ),
//                        "127.0.0.1",
//                        null
//                },
//                new Object[]{
//                        Collections.singletonList(
//                                new SessionEndpoint()
//                        ),
//                        "127.0.0.1",
//                        -1
//                },
//                new Object[]{
//                        Arrays.asList(
//                                null,
//                                new AdminEndpoint(),
//                                null
//                        ),
//                        "127.0.0.1",
//                        -10
//                }
//        };
//    }

//    @SuppressWarnings("unused")
//    public Object[] validObjectCaseData() {
//        return new Object[]{
//                new Object[]{new AuthenticationEndpoint(), "127.0.0.1", 9120},
//                new Object[]{new UserEndpoint(), "127.0.0.1", 9121},
//                new Object[]{new AdminEndpoint(), "127.0.0.1", 9122}
//        };
//    }

//    @SuppressWarnings("unused")
//    public Object[] validObjectsCaseData() {
//        return new Object[]{
//                new Object[]{
//                        Arrays.asList(
//                                new SessionEndpoint(),
//                                new UserEndpoint()
//                        ),
//                        "127.0.0.1",
//                        9123
//                },
//                new Object[]{
//                        Collections.singletonList(
//                                new AdminDataInterChangeEndpoint()
//                        ),
//                        "127.0.0.1",
//                        9124
//                },
//                new Object[]{
//                        Arrays.asList(
//                                new TaskEndpoint(),
//                                new ProjectEndpoint()
//                        ),
//                        "127.0.0.1",
//                        9125
//                }
//        };
//    }

}