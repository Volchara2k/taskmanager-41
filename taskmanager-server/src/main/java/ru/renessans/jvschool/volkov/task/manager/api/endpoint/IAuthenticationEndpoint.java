package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IEndpoint;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserLimitedDTO;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
@SuppressWarnings("unused")
public interface IAuthenticationEndpoint extends IEndpoint {

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    UserLimitedDTO signUpUser(
            @Nullable SessionDTO sessionDTO,
            @Nullable String login,
            @Nullable String password
    );

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    UserLimitedDTO signUpUserWithFirstName(
            @Nullable SessionDTO sessionDTO,
            @Nullable String login,
            @Nullable String password,
            @Nullable String firstName
    );

}