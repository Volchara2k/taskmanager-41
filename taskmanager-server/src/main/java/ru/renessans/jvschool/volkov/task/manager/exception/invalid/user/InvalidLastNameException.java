package ru.renessans.jvschool.volkov.task.manager.exception.invalid.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidLastNameException extends AbstractException {

    @NotNull
    private static final String EMPTY_LAST_NAME = "Ошибка! Параметр \"фамилия\" отсутствует!\n";

    public InvalidLastNameException() {
        super(EMPTY_LAST_NAME);
    }

}