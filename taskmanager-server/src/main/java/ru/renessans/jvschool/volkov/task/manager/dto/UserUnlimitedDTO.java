package ru.renessans.jvschool.volkov.task.manager.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;

import javax.xml.bind.annotation.XmlType;

@Data
@XmlType
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class UserUnlimitedDTO extends UserLimitedDTO {

    private static final long serialVersionUID = 1L;

    @NotNull
    private String passwordHash = "";

    @NotNull
    private UserRole role = UserRole.USER;

    @NotNull
    private Boolean lockdown = false;

    @NotNull
    @Override
    public String toString() {
        @NotNull final StringBuilder result = new StringBuilder();
        result.append("Логин: ").append(super.getLogin());
        if (!StringUtils.isEmpty(super.getFirstName()))
            result.append(", имя: ").append(super.getFirstName()).append("\n");
        if (!StringUtils.isEmpty(super.getLastName()))
            result.append(", фамилия: ").append(super.getLastName()).append("\n");
        result.append("\nРоль: ").append(getRole().getTitle()).append("\n");
        result.append("\nИдентификатор: ").append(super.getId()).append("\n");
        return result.toString();
    }

}