package ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidPortException extends AbstractException {

    @NotNull
    private static final String EMPTY_PORT = "Ошибка! Параметр \"port\" отсутствует!\n";

    public InvalidPortException() {
        super(EMPTY_PORT);
    }

}