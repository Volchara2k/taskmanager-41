package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IEndpoint;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@SuppressWarnings("unused")
public interface IProjectEndpoint extends IEndpoint {

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    ProjectDTO addProject(
            @Nullable SessionDTO sessionDTO,
            @Nullable String title,
            @Nullable String description
    );

    @WebMethod
    @WebResult(name = "updatedProject", partName = "updatedProject")
    @Nullable
    ProjectDTO updateProjectByIndex(
            @Nullable SessionDTO sessionDTO,
            @Nullable Integer index,
            @Nullable String title,
            @Nullable String description
    );

    @WebMethod
    @WebResult(name = "updatedProject", partName = "updatedProject")
    @Nullable
    ProjectDTO updateProjectById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id,
            @Nullable String title,
            @Nullable String description
    );

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    int deleteProjectById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id
    );

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    int deleteProjectByIndex(
            @Nullable SessionDTO sessionDTO,
            @Nullable Integer index
    );

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    int deleteProjectByTitle(
            @Nullable SessionDTO sessionDTO,
            @Nullable String title
    );

    @WebMethod
    @WebResult(name = "deletedProjects", partName = "deletedProjects")
    int deleteAllProjects(
            @Nullable SessionDTO sessionDTO
    );

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    ProjectDTO getProjectById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id
    );

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    ProjectDTO getProjectByIndex(
            @Nullable SessionDTO sessionDTO,
            @Nullable Integer index
    );

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    ProjectDTO getProjectByTitle(
            @Nullable SessionDTO sessionDTO,
            @Nullable String title
    );

    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @NotNull
    Collection<ProjectDTO> getAllProjects(
            @Nullable SessionDTO sessionDTO
    );

}