package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IEndpoint;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
@SuppressWarnings("unused")
public interface ISessionEndpoint extends IEndpoint {

    @WebMethod
    @WebResult(name = "openedSession", partName = "openedSession")
    @Nullable
    SessionDTO openSession(
            @Nullable String login,
            @Nullable String password
    );

    @WebMethod
    @WebResult(name = "session", partName = "session")
    SessionDTO closeSession(
            @Nullable SessionDTO sessionDTO
    );

    @WebMethod
    @WebResult(name = "session", partName = "session")
    @Nullable
    SessionDTO validateSession(
            @Nullable SessionDTO sessionDTO
    );

    @WebMethod
    @WebResult(name = "session", partName = "session")
    @Nullable
    SessionDTO validateSessionWithCommandRole(
            @Nullable SessionDTO sessionDTO,
            @Nullable UserRole commandRole
    );

    @WebMethod
    @WebResult(name = "sessionValidState", partName = "sessionValidState")
    @NotNull
    SessionValidState verifyValidSessionState(
            @Nullable SessionDTO sessionDTO
    );

    @WebMethod
    @WebResult(name = "permissionValidState", partName = "permissionValidState")
    @NotNull
    PermissionValidState verifyValidPermissionState(
            @Nullable SessionDTO sessionDTO,
            @Nullable UserRole commandRole
    );

}