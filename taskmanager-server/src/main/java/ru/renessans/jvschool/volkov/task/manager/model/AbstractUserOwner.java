package ru.renessans.jvschool.volkov.task.manager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.Status;

import javax.persistence.*;

@Getter
@Setter
@SuperBuilder
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwner extends AbstractModel {

    @NotNull
    @Column(
            nullable = false,
            length = 50
    )
    private String title = "";

    @NotNull
    @Column(
            nullable = false,
            length = 150
    )
    private String description = "";

    @Nullable
    @Column
    private String userId;

    @NotNull
    private TimeFrame timeFrame = new TimeFrame();

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

}