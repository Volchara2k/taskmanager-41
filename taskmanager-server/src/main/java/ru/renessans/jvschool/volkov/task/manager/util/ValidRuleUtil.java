package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

@UtilityClass
public final class ValidRuleUtil {

    public boolean isNullOrEmpty(@Nullable final Integer aInteger) {
        return aInteger == null || aInteger < 0;
    }

    public boolean isNullOrEmpty(@Nullable final Long aLong) {
        return aLong == null || aLong <= 0L;
    }

    public boolean isNullOrEmpty(@Nullable final Collection<?> aCollection) {
        if (aCollection == null || aCollection.isEmpty()) return true;
        return aCollection.stream().allMatch(ValidRuleUtil::isNullOrEmpty);
    }

    private boolean isNullOrEmpty(@Nullable final Object aObject) {
        return aObject == null;
    }

}