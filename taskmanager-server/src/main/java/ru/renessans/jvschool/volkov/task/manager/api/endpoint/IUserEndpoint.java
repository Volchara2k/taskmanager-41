package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IEndpoint;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
@SuppressWarnings("unused")
public interface IUserEndpoint extends IEndpoint {

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    UserLimitedDTO getUser(
            @Nullable SessionDTO sessionDTO
    );

    @WebMethod
    @WebResult(name = "userRole", partName = "userRole")
    @NotNull
    UserRole getUserRole(
            @Nullable SessionDTO sessionDTO
    );

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    UserLimitedDTO editProfile(
            @Nullable SessionDTO sessionDTO,
            @Nullable String firstName
    );

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    UserLimitedDTO editProfileWithLastName(
            @Nullable SessionDTO sessionDTO,
            @Nullable String firstName,
            @Nullable String lastName
    );

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    UserLimitedDTO updatePassword(
            @Nullable SessionDTO sessionDTO,
            @Nullable String newPassword
    );

}