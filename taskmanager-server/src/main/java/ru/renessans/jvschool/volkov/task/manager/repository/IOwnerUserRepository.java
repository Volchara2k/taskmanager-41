package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;

import java.util.List;

@NoRepositoryBean
public interface IOwnerUserRepository<E extends AbstractModel> extends IRepository<E> {

    @NotNull
    @Query("FROM #{#entityName} WHERE user.id = ?1 ORDER BY creationDate")
    List<E> getAllOwnerUser(
            @NotNull String userId
    );

    @Nullable
    @Query("FROM #{#entityName} WHERE user.id = ?1 AND id = ?2 ORDER BY creationDate")
    E getOwnerUserById(
            @NotNull String userId,
            @NotNull String id
    );

    @Nullable
    @Query("FROM #{#entityName} WHERE user.id = ?1 AND title = ?2 ORDER BY creationDate")
    E getOwnerUserByTitle(
            @NotNull String userId,
            @NotNull String title
    );

    @Query("SELECT COUNT(*) FROM #{#entityName} WHERE user.id = ?1")
    long countOwnerUser(
            @NotNull String userId
    );

}