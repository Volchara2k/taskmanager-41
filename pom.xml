<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>ru.renessans.jvschool.volkov.task.manager</groupId>
    <artifactId>taskmanager</artifactId>
    <version>1.0.41</version>
    <name>Task Manager</name>

    <packaging>pom</packaging>

    <developers>
        <developer>
            <id>Volkov</id>
            <name>Valery Volkov</name>
            <email>volkov.valery2013@yandex.ru</email>
        </developer>
    </developers>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <path.category>ru.renessans.jvschool.volkov.task.manager.marker</path.category>
        <lombock.version>1.18.16</lombock.version>
        <jetbrains.annotations.version>20.1.0</jetbrains.annotations.version>
        <junit.version>4.12</junit.version>
        <junit.params.version>1.1.0</junit.params.version>
        <junit.jupiter.version>5.7.0</junit.jupiter.version>
        <cxf.codegen.plugin.version>3.2.7</cxf.codegen.plugin.version>
        <enunciate.plugin.version>2.12.1</enunciate.plugin.version>
        <jackson.version>2.9.8</jackson.version>
        <spring.version>5.0.8.RELEASE</spring.version>
        <spring.data.jpa.version>2.0.7.RELEASE</spring.data.jpa.version>
        <hibernate.version>5.3.7.Final</hibernate.version>
        <mysql.connector.version>5.1.47</mysql.connector.version>
        <postgresql.version>42.2.18</postgresql.version>
        <hsqldb.version>2.5.1</hsqldb.version>
        <maven.compiler.plugin.version>2.5.1</maven.compiler.plugin.version>
        <maven.surefire.plugin>2.21.0</maven.surefire.plugin>
        <maven.dependency.plugin.version>3.1.2</maven.dependency.plugin.version>
        <maven.resources.plugin.version>3.0.2</maven.resources.plugin.version>
        <maven.jar.plugin.version>3.2.0</maven.jar.plugin.version>
        <cobertura.plugin.version>2.7</cobertura.plugin.version>
    </properties>

    <modules>
        <module>taskmanager-server</module>
        <module>taskmanager-client</module>
        <module>taskmanager-webapp</module>
        <module>taskmanager-swagger</module>
        <module>taskmanager-rest-client</module>
        <module>taskmanager-soap-client</module>
    </modules>

    <dependencies>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring.version}</version>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombock.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.jetbrains</groupId>
            <artifactId>annotations</artifactId>
            <version>${jetbrains.annotations.version}</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>pl.pragmatists</groupId>
            <artifactId>JUnitParams</artifactId>
            <version>${junit.params.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>${junit.jupiter.version}</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <profiles>

        <profile>
            <id>Skip-tests-run</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <test.category>no-tests</test.category>
            </properties>
        </profile>

        <profile>
            <id>Integration-tests-run</id>
            <properties>
                <test.category>
                    ${path.category}.IntegrationImplementation
                </test.category>
            </properties>
        </profile>

        <profile>
            <id>Negative-and-positive-tests-run</id>
            <properties>
                <test.category>
                    ${path.category}.PositiveImplementation,
                    ${path.category}.NegativeImplementation
                </test.category>
            </properties>
        </profile>

        <profile>
            <id>Negative-tests-run</id>
            <properties>
                <test.category>${path.category}.NegativeImplementation</test.category>
            </properties>
        </profile>

        <profile>
            <id>Positive-tests-run</id>
            <properties>
                <test.category>${path.category}.PositiveImplementation</test.category>
            </properties>
        </profile>

        <profile>
            <id>Repository-tests-run</id>
            <properties>
                <test.category>${path.category}.RepositoryImplementation</test.category>
            </properties>
        </profile>

        <profile>
            <id>Service-tests-run</id>
            <properties>
                <test.category>${path.category}.ServiceImplementation</test.category>
            </properties>
        </profile>

        <profile>
            <id>Utility-tests-run</id>
            <properties>
                <test.category>${path.category}.UtilityImplementation</test.category>
            </properties>
        </profile>

    </profiles>

    <build>
        <plugins>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven.compiler.plugin.version}</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven.surefire.plugin}</version>
                <configuration>
                    <groups>${test.category}</groups>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>cobertura-maven-plugin</artifactId>
                <version>${cobertura.plugin.version}</version>
            </plugin>

        </plugins>

    </build>

</project>